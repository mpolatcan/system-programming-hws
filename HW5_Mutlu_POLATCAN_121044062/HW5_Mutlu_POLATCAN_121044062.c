/* CSE 244 - System Programming
   Mutlu POLATCAN - 121044062
   HW5 - grepFromDirTh with mutex and semaphore */
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <pthread.h>

#define MAX_PATH 256
#define ARG_NUM 3 
#define OPEN_FILE O_WRONLY | O_APPEND
#define CREATE_FILE O_WRONLY | O_CREAT | O_APPEND
#define USER_PERMISSIONS S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH

typedef struct {
    char *cpFilename;
    char *cpGivenString;
    int pipeFD;
} argStruct;

static pthread_mutex_t locker = PTHREAD_MUTEX_INITIALIZER;

void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize);
void readFileContent(int iFileDes, char **strArrFileContent, int iRowSize, int iColSize);
void findGivenString(char **strArrFileContent, char *cpFilename, char *cpGivenString, int iRowSize, int iColSize,
                     int pipeFD);
void getSubstring(char *cpSubstr, char *cpToken, int iBeginIndex, int iEndIndex);
int readLine(int iFileDes, char *cpBuffer, int iNBytes);
ssize_t writeLine(int iFileDes, void *vpBuf, size_t size);
void writeToLogFile(char *cpGivenString, char *cpFilename, int *intArrFoundedNum, int **intArrLineColumnNums, int iRowSize);
void * wordFinder(void *args);
pid_t r_wait(int *stat_loc);
void traverseDirectories(char *cpCurDirPath, char *cpGivenString);

pthread_t *threadId;
pthread_attr_t threadAttr;    
argStruct *args;
char **cpTmpFilename,
     **cpTmpDirname;
struct dirent *sdirpEntry,
              **sdirpDirs,
              **sdirpFiles;
int iDirNum,
    iFileNum;
DIR *dir;

static void terminateProg(int signo) {
  int iLogFileDes, i, j;

  iLogFileDes = open("gfdLog.txt", O_APPEND | O_WRONLY);
  fprintf(stderr, "\nProgram has catch \"CTRL-C\" signal!\n");
  writeLine(iLogFileDes, "Program has catched \"CTRL-C\" signal !\n\n", 
  strlen("Program has catched \"CTRL-C\" signal !\n\n"));
  close(iLogFileDes);

  for (i = 0; i < iFileNum; ++i)
    free(cpTmpFilename[i]);
  for (j = 0; j < iDirNum; ++j)
    free(cpTmpDirname[j]);

    if (iFileNum != 0) {
        free(cpTmpFilename);
        free(threadId);    
        free(args);
        free(sdirpFiles);
    }

    if (iDirNum != 0) {
        free(cpTmpDirname);
        free(sdirpDirs);
    }
    closedir(dir);
    pthread_attr_destroy(&threadAttr);
    pthread_mutex_destroy(&locker);
    
    exit(1);
}

int main(int argc, char *argv[])
{
    if (argc < 3)
        fprintf(stderr,"Usage: %s [path] [string]\n",argv[0]);
    else
        traverseDirectories(argv[1], argv[2]);
    return 0;
}

pid_t r_wait(int *stat_loc) {
    int retval;

    while (((retval = wait(stat_loc)) == -1) && (errno == EINTR)) ;

    return retval;
}

void traverseDirectories(char *cpCurDirPath, char *cpGivenString) {
    pid_t childpid = -1;
    char chArrNewDirPath[MAX_PATH];
    int i,
        j = 0,
        k = 0,
        iLogFileDes,        
        arrPipeFD[2],
        iRequiredChNum,
        occurrenceNum,
        totalOccurrenceNum = 0;

    iDirNum = 0;
    iFileNum = 0;

    signal(SIGINT, terminateProg);

    if (pipe(arrPipeFD) == -1) {
        fprintf(stderr, "Failed to open pipe!\n");
        return; 
    }

    if (!(dir = opendir(cpCurDirPath))) {
        fprintf(stderr, "Failed to open dir %s\n", cpCurDirPath);
        return;
    }

    pthread_attr_init(&threadAttr);
    pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);
        
    /* Counting files and directories numbers */
    while ((sdirpEntry = readdir(dir)) != NULL) {
        if (sdirpEntry->d_type == DT_REG)
            ++iFileNum;
        else if (sdirpEntry->d_type == DT_DIR &&
                   strcmp(sdirpEntry->d_name, ".") != 0 &&
                   strcmp(sdirpEntry->d_name, "..") != 0)
            ++iDirNum;
    }

    rewinddir(dir);
    
    if (iFileNum != 0) {
        sdirpFiles = (struct dirent **)calloc(iFileNum, sizeof(struct dirent *));
        args = (argStruct *)calloc(iFileNum, sizeof(argStruct));
        threadId = (pthread_t *)calloc(iFileNum, sizeof(pthread_t));
        cpTmpFilename = (char **)calloc(iFileNum, sizeof(char *));
    }

    if (iDirNum != 0) {
        sdirpDirs = (struct dirent **)calloc(iDirNum, sizeof(struct dirent *));
        cpTmpDirname = (char **)calloc(iDirNum, sizeof(char *));
    }

    /* Stroing entries to files and dirs arrays */
    while ((sdirpEntry = readdir(dir)) != NULL) {
        if (sdirpEntry->d_type == DT_REG) {
            sdirpFiles[j] = sdirpEntry;
            ++j;
        } else if (sdirpEntry->d_type == DT_DIR &&
                   strcmp(sdirpEntry->d_name, ".") != 0 &&
                   strcmp(sdirpEntry->d_name, "..") != 0) {
            sdirpDirs[k] = sdirpEntry;
            ++k;
        }
    }

    for (i = 0; i < iDirNum; ++i) {
        cpTmpDirname[i] = (char *)calloc(MAX_PATH, sizeof(char));
        sprintf(cpTmpDirname[i], "%s%s/", cpCurDirPath, sdirpDirs[i]->d_name);
    }

    for (i = 0; i < iFileNum; ++i) {
        /* Setting the filename's absolute path */
        iRequiredChNum = snprintf(chArrNewDirPath, 0, "%s%s", cpCurDirPath, sdirpFiles[i]->d_name);
        cpTmpFilename[i] = (char *)calloc(iRequiredChNum + 1, sizeof(char));
        sprintf(cpTmpFilename[i], "%s%s", cpCurDirPath, sdirpFiles[i]->d_name);
        /* ------------------------------------ */

        /* Setting arguments */
        args[i].cpGivenString = cpGivenString;
        args[i].pipeFD = arrPipeFD[1];
        args[i].cpFilename = cpTmpFilename[i];
        /* ----------------- */

        pthread_create(&threadId[i], &threadAttr, wordFinder, (void *)&args[i]);
    }

    pthread_attr_destroy(&threadAttr);
    for (i = 0; i < iFileNum; ++i) {
        pthread_join(threadId[i], NULL);
        read(arrPipeFD[0], &occurrenceNum, sizeof(int));
        totalOccurrenceNum += occurrenceNum;
    }

    for (i = 0; i < iDirNum; ++i) {
        if ((childpid = fork()) == 0)
            break;
    }

    while (r_wait(NULL) > 0);

    if (childpid == 0) {
        traverseDirectories(cpTmpDirname[i], cpGivenString);
        exit(1);
    } else {
        for (i = 0; i < iFileNum; ++i)
             free(cpTmpFilename[i]);

        for (j = 0; j < iDirNum; ++j) 
             free(cpTmpDirname[j]);

        if (iFileNum != 0) {
            free(cpTmpFilename);
            free(threadId);    
            free(args);
            free(sdirpFiles);
        }

        if (iDirNum != 0) {
            free(cpTmpDirname);
            free(sdirpDirs);
        }

        closedir(dir);
    }
}

/* This function finds given word in given file and writes to log file */
void * wordFinder(void *args)
{
    argStruct *arguments = (argStruct *)args;
    int i, iFileDes, iRowSize, iColSize;
    char **strArrFileContent;

    pthread_mutex_lock(&locker);
    while ((iFileDes = open(arguments->cpFilename, O_RDONLY)) == -1 && errno == EINTR);

    if (iFileDes == -1)
        perror("File can't be opened!");
    else {
        /* Find row size and max column size of file */
        findRowAndColSize(iFileDes, &iRowSize, &iColSize);

        if (iRowSize != 0)
            strArrFileContent = (char **)calloc(iRowSize + 1, sizeof(char *));
        for (i = 0; i < iRowSize; ++i)
            strArrFileContent[i] = (char *)calloc(iColSize + 1, sizeof(char));

        /* Read contents of file */
        readFileContent(iFileDes, strArrFileContent, iRowSize, iColSize);

        /* Find occurrences of given string and write information to log file */
        findGivenString(strArrFileContent, arguments->cpFilename, arguments->cpGivenString, 
            iRowSize, iColSize, arguments->pipeFD);

        for (i = 0; i < iRowSize; ++i)
            free(strArrFileContent[i]);
        if (iRowSize != 0)
            free(strArrFileContent);

        close(iFileDes);
    }
    pthread_mutex_unlock(&locker);
    pthread_exit((void *)0);
}

/* Find occurrences of given string, print to information to terminal and log file */
void findGivenString(char **strArrFileContent, char *cpFilename, char *cpGivenString, int iRowSize, int iColSize,
                    int pipeFD) {
    int i, 
        j,
        iFoundedNum = 0, 
        iTotalFoundedNum = 0,
        iCapacity = 0;  
    int **intArrLineColumnNums, *intArrFoundedNum;
    char *cpSubStr;

    cpSubStr = (char *)calloc(strlen(cpGivenString) + 1, sizeof(char));
    intArrLineColumnNums = (int **)calloc(iRowSize + 1, sizeof(int *)); // allocating to store column numbers of occurrences in a line
    intArrFoundedNum = (int *)calloc(iRowSize + 1, sizeof(int)); // allocating to store occurence number of given string in a line

    for (i = 0; i < iRowSize; ++i) {
        intArrLineColumnNums[i] = (int *)calloc(iCapacity + 20, sizeof(int));
        iCapacity += 20;
        if (strcmp(strArrFileContent[i], "\n") != 0) {      
            for (j = 0; j < strlen(strArrFileContent[i]); ++j) {
                getSubstring(cpSubStr, strArrFileContent[i], j, j + strlen(cpGivenString) - 1);
                if (strcmp(cpGivenString, cpSubStr) == 0) {
                    if (iFoundedNum == iCapacity) {
                        intArrLineColumnNums[i] = (int *)realloc(intArrLineColumnNums[i], (iCapacity + 20)*sizeof(int));
                        iCapacity += 20;
                    }
                    intArrLineColumnNums[i][iFoundedNum] = j + 1;
                    ++iFoundedNum;
                }
            }

            intArrFoundedNum[i] = iFoundedNum;
        }
        
        iCapacity = 0;
        iFoundedNum = 0;
    }
    
    for (i = 0; i < iRowSize; ++i)
        iTotalFoundedNum += intArrFoundedNum[i];
    
    if (iTotalFoundedNum != 0) {
        printf("\nOccurrences in file %s for \"%s\" string:\n", cpFilename, cpGivenString);
        printf("-------------------------------------------------------\n");
    }

    for (i = 0; i < iRowSize; ++i) {
        for (j = 0; j < intArrFoundedNum[i]; ++j) {
            if (j == 0) 
                printf("Line: %d | Number of occurrences in line: %d | Columns: { %d", i + 1, 
                    intArrFoundedNum[i], intArrLineColumnNums[i][j]);
            else
                printf(", %d", intArrLineColumnNums[i][j]);
        }
        if (intArrFoundedNum[i] != 0)
            printf(" }\n");
    }

    if (iTotalFoundedNum != 0)
        printf("\nTotal number of occurrences in file for string \"%s\": %d\n", cpGivenString, iTotalFoundedNum);

    if (iTotalFoundedNum == 0)
        printf("\nNo occurrences found in file %s for \"%s\"!\n", cpFilename, cpGivenString);

    writeToLogFile(cpGivenString, cpFilename, intArrFoundedNum, intArrLineColumnNums, iRowSize);

    for (i = 0; i < iRowSize; ++i)
        free(intArrLineColumnNums[i]);

    printf("-----------------------------------------------------------------------\n");

    /* write total occurrence number into pipe */
    write(pipeFD, &iTotalFoundedNum, sizeof(int));
    
    free(intArrLineColumnNums);
    free(intArrFoundedNum);
    free(cpSubStr);
}

/* Writes search information to .log file */
void writeToLogFile(char *cpGivenString, char *cpFilename, int *intArrFoundedNum, int **intArrLineColumnNums, int iRowSize) {
    int i, 
        j, 
        iLogFileDes, 
        iRequiredChNum, 
        iTotalFoundedNum = 0;
    char chArrTmpBuffer[20];

    iLogFileDes = open("gfdLog.txt", CREATE_FILE, USER_PERMISSIONS);

    for (i = 0; i < iRowSize; ++i)
        iTotalFoundedNum += intArrFoundedNum[i];

    writeLine(iLogFileDes, "<---- Filename: ", 16 * sizeof(char));
    writeLine(iLogFileDes, cpFilename, strlen(cpFilename));
    writeLine(iLogFileDes, "     Searched String: ", 22 * sizeof(char));
    writeLine(iLogFileDes, cpGivenString, strlen(cpGivenString) * sizeof(char));
    writeLine(iLogFileDes, " ---->", 6 * sizeof(char));
    writeLine(iLogFileDes, "\n", 1 * sizeof(char));

    for (i = 0; i < iRowSize; ++i) {
        if (intArrFoundedNum[i] != 0) {
            writeLine(iLogFileDes, "Line", 4 * sizeof(char)); 
            sprintf(chArrTmpBuffer, " %d", i + 1);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
            writeLine(iLogFileDes, " | ", 3 * sizeof(char));
            writeLine(iLogFileDes, "Number of occurences in line: ", 30 * sizeof(char));
            sprintf(chArrTmpBuffer, "%d", intArrFoundedNum[i]);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
            writeLine(iLogFileDes, " | ", 3 * sizeof(char));
            writeLine(iLogFileDes, "Columns: { ", 11 * sizeof(char));
            sprintf(chArrTmpBuffer, "%d", intArrLineColumnNums[i][0]);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
            
            for (j = 1; j < intArrFoundedNum[i]; ++j) {
                writeLine(iLogFileDes, ", ", 2 * sizeof(char));
                sprintf(chArrTmpBuffer, "%d", intArrLineColumnNums[i][j]);
                writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
            }
        
            if (intArrFoundedNum[i] != 0)
                writeLine(iLogFileDes, " }\n", 3 * sizeof(char));
        }
    }

    if (iTotalFoundedNum != 0) {
        writeLine(iLogFileDes, "Total number of occurrences in file: ", 37 * sizeof(char));
        sprintf(chArrTmpBuffer, "%d", iTotalFoundedNum);
        writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
        writeLine(iLogFileDes, "\n", 1 * sizeof(char));
    } else 
        writeLine(iLogFileDes, "No occurrences found in file\n", 30 * sizeof(char));
    
    writeLine(iLogFileDes, "\n*", 1 * sizeof(char));

    close(iLogFileDes);
}

/* Returns substring of given string according to begin and end index */
void getSubstring(char *cpSubStr, char *cpToken, int iBeginIndex, int iEndIndex) {
    int i, 
        j;

    for (i = 0, j = iBeginIndex; i < (iEndIndex - iBeginIndex) + 1, j < iEndIndex + 1; ++i, ++j)
        cpSubStr[i] = cpToken[j];
}

/* Reads contents of file according to row size and column size information */
void readFileContent(int iFileDes, char **strArrFileContent, int iRowSize, int iColSize) {
    int i;

    lseek(iFileDes, 0, SEEK_SET);

    for (i = 0; i < iRowSize; ++i) 
        readLine(iFileDes, strArrFileContent[i], iColSize);
}

/* Finds file's row size and max column size */
void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize) {
    char cNewline;
    int iTempColSize = 0;

    *piRowSize = 0;
    *piColSize = 0;

    while (read(iFileDes, &cNewline, 1)) {
        if (cNewline == '\n') {
            ++*piRowSize;
            
            if (*piColSize < iTempColSize)
                *piColSize = iTempColSize + 2;

            iTempColSize = 0;
        } else {
            ++iTempColSize;
        }
    }

    ++*piRowSize;
}

int readLine(int iFileDes, char *cpBuffer, int iNBytes) {
    int iNumRead = 0,
        iReturnValue;

    while (iNumRead < iNBytes - 1) {
        iReturnValue = read(iFileDes, cpBuffer + iNumRead, 1);
        if ((iReturnValue == -1) && (errno == EINTR))
            continue;
        if ((iReturnValue == 0) && (iNumRead == 0))
            return 0;
        if ((iReturnValue == 0))
            break;
        if (iReturnValue == -1)
            return -1;
        iNumRead++;
        if (cpBuffer[iNumRead-1] == '\n') {
            cpBuffer[iNumRead] = '\0';
            return iNumRead;
        }
    }

    errno = EINVAL;
    return -1;
}

ssize_t writeLine(int iFileDes, void *vpBuf, size_t size) {
    char *bufp;
    size_t bytestowrite;
    ssize_t byteswritten;
    size_t totalbytes;
    
    for (bufp = vpBuf, bytestowrite = size, totalbytes = 0;
         bytestowrite > 0;
         bufp += byteswritten, bytestowrite -= byteswritten) {
         byteswritten = write(iFileDes, bufp, bytestowrite);
        if ((byteswritten) == -1 && (errno != EINTR))
            return -1;
        if (byteswritten == -1)
            byteswritten = 0;
        totalbytes += byteswritten;
    }
    
    return totalbytes;
}