/* CSE 244 - System Programming
   Mutlu POLATCAN - 121044062
   HW6 - grepFromDirTh with shared memory and message queues */
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <pthread.h>
#include <semaphore.h>

#define MAX_PATH 256
#define ARG_NUM 3 
#define OPEN_FILE O_WRONLY | O_APPEND
#define CREATE_FILE O_WRONLY | O_CREAT | O_APPEND
#define SHARED_MEM_PERMISSIONS S_IRUSR | S_IWUSR
#define MSG_QUEUE_PERMISSIONS S_IRUSR | S_IWUSR
#define USER_PERMISSIONS S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH

typedef struct {
    char *cpFilename;
    char *cpGivenString;
    int msgqueueId;
} argStruct;

typedef struct {
    long mtype;
    int iFoundedNum;
} myMsg;

static pthread_mutex_t locker = PTHREAD_MUTEX_INITIALIZER;

void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize);
void readFileContent(int iFileDes, char **strArrFileContent, int iRowSize, int iColSize);
int findGivenString(char **strArrFileContent, char *cpFilename, char *cpGivenString, int iRowSize, int iColSize);
void getSubstring(char *cpSubstr, char *cpToken, int iBeginIndex, int iEndIndex);
int readLine(int iFileDes, char *cpBuffer, int iNBytes);
ssize_t writeLine(int iFileDes, void *vpBuf, size_t size);
void writeToLogFile(char *cpGivenString, char *cpFilename, int *intArrFoundedNum, int **intArrLineColumnNums, int iRowSize);
void * wordFinder(void *args);
pid_t r_wait(int *stat_loc);
void traverseDirectories(char *cpCurDirPath, char *cpGivenString);

key_t key;
pthread_t *threadId;
pthread_attr_t threadAttr;    
sem_t sharedSem;
argStruct *args;
char **cpTmpFilename,
     **cpTmpDirname;
struct dirent *sdirpEntry,
              **sdirpDirs,
              **sdirpFiles;
int iDirNum,
    iFileNum,
    id;
int *iTotalFoundedNum;
DIR *dir;

static void terminateProg(int signo) {
  int iLogFileDes, i, j;

  iLogFileDes = open("gfdLog.txt", O_APPEND | O_WRONLY);
  fprintf(stderr, "\nProgram has catch \"CTRL-C\" signal!\n");
  writeLine(iLogFileDes, "Program has catched \"CTRL-C\" signal !\n\n", 
  strlen("Program has catched \"CTRL-C\" signal !\n\n"));
  close(iLogFileDes);

  for (i = 0; i < iFileNum; ++i)
    free(cpTmpFilename[i]);
  for (j = 0; j < iDirNum; ++j)
    free(cpTmpDirname[j]);

    if (iFileNum != 0) {
        free(cpTmpFilename);
        free(threadId);    
        free(args);
        free(sdirpFiles);
    }

    if (iDirNum != 0) {
        free(cpTmpDirname);
        free(sdirpDirs);
    }
    closedir(dir);
    pthread_attr_destroy(&threadAttr);
    pthread_mutex_destroy(&locker);
    
    exit(1);
}

void traverseDirectories(char *cpCurDirPath, char *cpGivenString) {
    pid_t childpid = -1;
    char chArrNewDirPath[MAX_PATH];
    int i,
        j = 0,
        k = 0,
        msgqueueId,
        iLogFileDes,        
        arrPipeFD[2],
        iRequiredChNum,
        occurrenceNum,
        totalOccurrenceNum = 0;
    key_t msgqueueKey;
    myMsg result;

    iDirNum = 0;
    iFileNum = 0;

    signal(SIGINT, terminateProg);

    msgqueueKey = ftok(cpCurDirPath, 0);

    if ((msgqueueId = msgget(msgqueueKey, MSG_QUEUE_PERMISSIONS | IPC_CREAT | IPC_EXCL)) == -1) {
        fprintf(stderr, "Failed to create message queue\n");
        exit(1);
    }
    
    if (!(dir = opendir(cpCurDirPath))) {
        fprintf(stderr, "Failed to open dir %s\n", cpCurDirPath);
        exit(1);
    }

    pthread_attr_init(&threadAttr);
    pthread_attr_setdetachstate(&threadAttr, PTHREAD_CREATE_JOINABLE);
        
    /* Counting files and directories numbers */
    while ((sdirpEntry = readdir(dir)) != NULL) {
        if (sdirpEntry->d_type == DT_REG)
            ++iFileNum;
        else if (sdirpEntry->d_type == DT_DIR &&
                   strcmp(sdirpEntry->d_name, ".") != 0 &&
                   strcmp(sdirpEntry->d_name, "..") != 0)
            ++iDirNum;
    }

    rewinddir(dir);
    
    if (iFileNum != 0) {
        sdirpFiles = (struct dirent **)calloc(iFileNum, sizeof(struct dirent *));
        args = (argStruct *)calloc(iFileNum, sizeof(argStruct));
        threadId = (pthread_t *)calloc(iFileNum, sizeof(pthread_t));
        cpTmpFilename = (char **)calloc(iFileNum, sizeof(char *));
    }

    if (iDirNum != 0) {
        sdirpDirs = (struct dirent **)calloc(iDirNum, sizeof(struct dirent *));
        cpTmpDirname = (char **)calloc(iDirNum, sizeof(char *));
    }

    /* Stroing entries to files and dirs arrays */
    while ((sdirpEntry = readdir(dir)) != NULL) {
        if (sdirpEntry->d_type == DT_REG) {
            sdirpFiles[j] = sdirpEntry;
            ++j;
        } else if (sdirpEntry->d_type == DT_DIR &&
                   strcmp(sdirpEntry->d_name, ".") != 0 &&
                   strcmp(sdirpEntry->d_name, "..") != 0) {
            sdirpDirs[k] = sdirpEntry;
            ++k;
        }
    }

    for (i = 0; i < iDirNum; ++i) {
        cpTmpDirname[i] = (char *)calloc(MAX_PATH, sizeof(char));
        sprintf(cpTmpDirname[i], "%s%s/", cpCurDirPath, sdirpDirs[i]->d_name);
    }

    for (i = 0; i < iFileNum; ++i) {
        /* Setting the filename's absolute path */
        iRequiredChNum = snprintf(chArrNewDirPath, 0, "%s%s", cpCurDirPath, sdirpFiles[i]->d_name);
        cpTmpFilename[i] = (char *)calloc(iRequiredChNum + 1, sizeof(char));
        sprintf(cpTmpFilename[i], "%s%s", cpCurDirPath, sdirpFiles[i]->d_name);
        /* ------------------------------------ */

        /* Setting arguments */
        args[i].cpGivenString = cpGivenString;
        args[i].msgqueueId = msgqueueId;
        args[i].cpFilename = cpTmpFilename[i];
        /* ----------------- */

        pthread_create(&threadId[i], &threadAttr, wordFinder, (void *)&args[i]);
    }

    pthread_attr_destroy(&threadAttr);
    for (i = 0; i < iFileNum; ++i) {
        pthread_join(threadId[i], NULL);

        if (msgrcv(msgqueueId, &result, sizeof(int), 1, 0) == -1) {
            fprintf(stderr, "Message can't be received\n");
            exit(1);
        }

        totalOccurrenceNum += result.iFoundedNum;
    }

    sem_wait(&sharedSem);
    *iTotalFoundedNum += totalOccurrenceNum;
    sem_post(&sharedSem);

    for (i = 0; i < iFileNum; ++i)
        free(cpTmpFilename[i]);

    if (iFileNum != 0) {
        free(cpTmpFilename);
        free(threadId);    
        free(args);
        free(sdirpFiles);
    }

    if (iDirNum != 0)
        free(sdirpDirs);

    for (i = 0; i < iDirNum; ++i) {
        if ((childpid = fork()) == 0)
            break;
    }

    while (r_wait(NULL) > 0);

    if (childpid == 0) {
        closedir(dir);
        traverseDirectories(cpTmpDirname[i], cpGivenString);
        exit(1);
    } else {
        for (j = 0; j < iDirNum; ++j) 
            free(cpTmpDirname[j]);

        if (iDirNum != 0)
            free(cpTmpDirname);

        if (msgctl(msgqueueId, IPC_RMID, NULL) == -1) {
            fprintf(stderr, "Shared memory can't be removed!\n");
            exit(1);
        }

        closedir(dir);
    }
}

int main(int argc, char *argv[])
{
    if (argc < 3)
        fprintf(stderr,"Usage: %s [path] [string]\n",argv[0]);
    else {
        key = ftok(argv[1], 0);

        if ((id = shmget(key, sizeof(int), SHARED_MEM_PERMISSIONS | IPC_CREAT | IPC_EXCL)) == -1) {
            fprintf(stderr, "Couldn't get shared memory!\n");
            exit(1);
        }
        
        if ((iTotalFoundedNum = (int *)shmat(id, NULL, 0)) == (void *)-1) {
            fprintf(stderr, "Shared memory can't be attached!\n");
            exit(1);
        }

        if (sem_init(&sharedSem, 0, 1) == -1) {
            fprintf(stderr, "Semaphore can't be initialized!\n");
            exit(1);
        }

        *iTotalFoundedNum = 0;
        
        traverseDirectories(argv[1], argv[2]);
        
        printf("\nTotal occurrence: %d\n", *iTotalFoundedNum);

        if (shmdt(iTotalFoundedNum) == -1) {
            fprintf(stderr, "Shared memory can't be detached!\n");
            exit(1);
        }

        if (shmctl(id, IPC_RMID, NULL) == -1) {
            fprintf(stderr, "Shared memory can't be removed!\n");
            exit(1);
        }     

        if (sem_destroy(&sharedSem) == -1) {
            fprintf(stderr, "Semaphore can't be removed!\n");
            exit(1);
        }  
    }

    return 0;
}

pid_t r_wait(int *stat_loc) {
    int retval;

    while (((retval = wait(stat_loc)) == -1) && (errno == EINTR)) ;

    return retval;
}

/* This function finds given word in given file and writes to log file */
void * wordFinder(void *args)
{
    argStruct *arguments = (argStruct *)args;
    va_list ap;
    myMsg *result;
    int i, 
        iFileDes, 
        iRowSize, 
        iColSize, 
        iTotalFoundedNum,
        iRequiredChNum;
    char *foundedNum;
    char **strArrFileContent;

    pthread_mutex_lock(&locker);
    while ((iFileDes = open(arguments->cpFilename, O_RDONLY)) == -1 && errno == EINTR);

    if (iFileDes == -1)
        fprintf(stderr, "File %s can't be opened!\n", arguments->cpFilename);
    else {
        /* Find row size and max column size of file */
        findRowAndColSize(iFileDes, &iRowSize, &iColSize);

        if (iRowSize != 0)
            strArrFileContent = (char **)calloc(iRowSize + 1, sizeof(char *));
        for (i = 0; i < iRowSize; ++i)
            strArrFileContent[i] = (char *)calloc(iColSize + 1, sizeof(char));

        /* Read contents of file */
        readFileContent(iFileDes, strArrFileContent, iRowSize, iColSize);

        /* Find occurrences of given string and write information to log file */
        iTotalFoundedNum = findGivenString(strArrFileContent, arguments->cpFilename, arguments->cpGivenString, 
            iRowSize, iColSize);

        result = (myMsg *)malloc(sizeof(myMsg) + sizeof(int));
        result->iFoundedNum = iTotalFoundedNum;
        result->mtype = 1;

        if (msgsnd(arguments->msgqueueId, result, sizeof(int), 0) == -1) {
            fprintf(stderr, "Message can't be sended!\n");
            exit(1);
        }

        free(result);

        for (i = 0; i < iRowSize; ++i)
            free(strArrFileContent[i]);
        if (iRowSize != 0)
            free(strArrFileContent);

        close(iFileDes);
    }
    pthread_mutex_unlock(&locker);
    pthread_exit((void *)0);
}

/* Find occurrences of given string, print to information to terminal and log file */
int findGivenString(char **strArrFileContent, char *cpFilename, char *cpGivenString, 
                    int iRowSize, int iColSize) {
    int i, 
        j,
        iFoundedNum = 0, 
        iTotalFoundedNum = 0,
        iCapacity = 0;  
    int **intArrLineColumnNums, *intArrFoundedNum;
    char *cpSubStr;

    cpSubStr = (char *)calloc(strlen(cpGivenString), sizeof(char));
    intArrLineColumnNums = (int **)calloc(iRowSize, sizeof(int *)); // allocating to store column numbers of occurrences in a line
    intArrFoundedNum = (int *)calloc(iRowSize, sizeof(int)); // allocating to store occurence number of given string in a line

    for (i = 0; i < iRowSize; ++i) {
        intArrLineColumnNums[i] = (int *)calloc(iCapacity + 20, sizeof(int));
        iCapacity += 20;
        if (strcmp(strArrFileContent[i], "\n") != 0) {      
            for (j = 0; j < strlen(strArrFileContent[i]); ++j) {
                getSubstring(cpSubStr, strArrFileContent[i], j, j + strlen(cpGivenString) - 1);
                if (strcmp(cpGivenString, cpSubStr) == 0) {
                    if (iFoundedNum == iCapacity) {
                        intArrLineColumnNums[i] = (int *)realloc(intArrLineColumnNums[i], (iCapacity + 20)*sizeof(int));
                        iCapacity += 20;
                    }
                    intArrLineColumnNums[i][iFoundedNum] = j + 1;
                    ++iFoundedNum;
                }
            }

            intArrFoundedNum[i] = iFoundedNum;
        }
        
        iCapacity = 0;
        iFoundedNum = 0;
    }
    
    for (i = 0; i < iRowSize; ++i)
        iTotalFoundedNum += intArrFoundedNum[i];
    
    if (iTotalFoundedNum != 0) {
        printf("\nOccurrences in file %s for \"%s\" string:\n", cpFilename, cpGivenString);
        printf("-------------------------------------------------------\n");
    }

    for (i = 0; i < iRowSize; ++i) {
        for (j = 0; j < intArrFoundedNum[i]; ++j) {
            if (j == 0) 
                printf("Line: %d | Number of occurrences in line: %d | Columns: { %d", i + 1, 
                    intArrFoundedNum[i], intArrLineColumnNums[i][j]);
            else
                printf(", %d", intArrLineColumnNums[i][j]);
        }
        if (intArrFoundedNum[i] != 0)
            printf(" }\n");
    }

    if (iTotalFoundedNum != 0)
        printf("\nTotal number of occurrences in file for string \"%s\": %d\n", cpGivenString, iTotalFoundedNum);

    if (iTotalFoundedNum == 0)
        printf("\nNo occurrences found in file %s for \"%s\"!\n", cpFilename, cpGivenString);

    writeToLogFile(cpGivenString, cpFilename, intArrFoundedNum, intArrLineColumnNums, iRowSize);

    for (i = 0; i < iRowSize; ++i)
        free(intArrLineColumnNums[i]);

    printf("-----------------------------------------------------------------------\n");
    
    free(intArrLineColumnNums);
    free(intArrFoundedNum);
    free(cpSubStr);

    return iTotalFoundedNum;
}

/* Writes search information to .log file */
void writeToLogFile(char *cpGivenString, char *cpFilename, int *intArrFoundedNum, int **intArrLineColumnNums, int iRowSize) {
    int i, 
        j, 
        iLogFileDes, 
        iRequiredChNum, 
        iTotalFoundedNum = 0;
    char chArrTmpBuffer[20];

    iLogFileDes = open("gfdLog.txt", CREATE_FILE, USER_PERMISSIONS);

    for (i = 0; i < iRowSize; ++i)
        iTotalFoundedNum += intArrFoundedNum[i];

    writeLine(iLogFileDes, "<---- Filename: ", 16 * sizeof(char));
    writeLine(iLogFileDes, cpFilename, strlen(cpFilename));
    writeLine(iLogFileDes, "     Searched String: ", 22 * sizeof(char));
    writeLine(iLogFileDes, cpGivenString, strlen(cpGivenString) * sizeof(char));
    writeLine(iLogFileDes, " ---->", 6 * sizeof(char));
    writeLine(iLogFileDes, "\n", 1 * sizeof(char));

    for (i = 0; i < iRowSize; ++i) {
        if (intArrFoundedNum[i] != 0) {
            writeLine(iLogFileDes, "Line", 4 * sizeof(char)); 
            sprintf(chArrTmpBuffer, " %d", i + 1);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
            writeLine(iLogFileDes, " | ", 3 * sizeof(char));
            writeLine(iLogFileDes, "Number of occurences in line: ", 30 * sizeof(char));
            sprintf(chArrTmpBuffer, "%d", intArrFoundedNum[i]);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
            writeLine(iLogFileDes, " | ", 3 * sizeof(char));
            writeLine(iLogFileDes, "Columns: { ", 11 * sizeof(char));
            sprintf(chArrTmpBuffer, "%d", intArrLineColumnNums[i][0]);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
            
            for (j = 1; j < intArrFoundedNum[i]; ++j) {
                writeLine(iLogFileDes, ", ", 2 * sizeof(char));
                sprintf(chArrTmpBuffer, "%d", intArrLineColumnNums[i][j]);
                writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
            }
        
            if (intArrFoundedNum[i] != 0)
                writeLine(iLogFileDes, " }\n", 3 * sizeof(char));
        }
    }

    if (iTotalFoundedNum != 0) {
        writeLine(iLogFileDes, "Total number of occurrences in file: ", 37 * sizeof(char));
        sprintf(chArrTmpBuffer, "%d", iTotalFoundedNum);
        writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer) * sizeof(char));
        writeLine(iLogFileDes, "\n", 1 * sizeof(char));
    } else 
        writeLine(iLogFileDes, "No occurrences found in file\n", 30 * sizeof(char));
    
    writeLine(iLogFileDes, "\n*", 1 * sizeof(char));

    close(iLogFileDes);
}

/* Returns substring of given string according to begin and end index */
void getSubstring(char *cpSubStr, char *cpToken, int iBeginIndex, int iEndIndex) {
    int i, 
        j;

    for (i = 0, j = iBeginIndex; i < (iEndIndex - iBeginIndex) + 1, j < iEndIndex + 1; ++i, ++j)
        cpSubStr[i] = cpToken[j];
}

/* Reads contents of file according to row size and column size information */
void readFileContent(int iFileDes, char **strArrFileContent, int iRowSize, int iColSize) {
    int i;

    lseek(iFileDes, 0, SEEK_SET);

    for (i = 0; i < iRowSize; ++i) 
        readLine(iFileDes, strArrFileContent[i], iColSize);
}

/* Finds file's row size and max column size */
void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize) {
    char cNewline;
    int iTempColSize = 0;

    *piRowSize = 0;
    *piColSize = 0;

    while (read(iFileDes, &cNewline, 1)) {
        if (cNewline == '\n') {
            ++*piRowSize;
            
            if (*piColSize < iTempColSize)
                *piColSize = iTempColSize + 2;

            iTempColSize = 0;
        } else {
            ++iTempColSize;
        }
    }

    ++*piRowSize;
}

int readLine(int iFileDes, char *cpBuffer, int iNBytes) {
    int iNumRead = 0,
        iReturnValue;

    while (iNumRead < iNBytes - 1) {
        iReturnValue = read(iFileDes, cpBuffer + iNumRead, 1);
        if ((iReturnValue == -1) && (errno == EINTR))
            continue;
        if ((iReturnValue == 0) && (iNumRead == 0))
            return 0;
        if ((iReturnValue == 0))
            break;
        if (iReturnValue == -1)
            return -1;
        iNumRead++;
        if (cpBuffer[iNumRead-1] == '\n') {
            cpBuffer[iNumRead] = '\0';
            return iNumRead;
        }
    }

    errno = EINVAL;
    return -1;
}

ssize_t writeLine(int iFileDes, void *vpBuf, size_t size) {
    char *bufp;
    size_t bytestowrite;
    ssize_t byteswritten;
    size_t totalbytes;
    
    for (bufp = vpBuf, bytestowrite = size, totalbytes = 0;
         bytestowrite > 0;
         bufp += byteswritten, bytestowrite -= byteswritten) {
         byteswritten = write(iFileDes, bufp, bytestowrite);
        if ((byteswritten) == -1 && (errno != EINTR))
            return -1;
        if (byteswritten == -1)
            byteswritten = 0;
        totalbytes += byteswritten;
    }
    
    return totalbytes;
}