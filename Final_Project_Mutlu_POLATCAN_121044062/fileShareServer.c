/* CSE 244 - System Programming 
   Final Project - FTP Server
   Mutlu POLATCAN - 121044062 */
   
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <limits.h>
#include <sys/wait.h>
#include <semaphore.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <pthread.h>

#define ANSI_COLOR_RED     "\x1b[1;31m"
#define ANSI_COLOR_YELLOW  "\x1b[1;33m"
#define ANSI_COLOR_CYAN    "\x1b[1;36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define MAX_CLIENT_REQUEST_CH_NUM 64
#define MAX_ONLINE_USERS 100
#define MAX_CLIENT_ID_CH_NUM 64
#define MAX_CLIENT_PASS_CH_NUM 128
#define MAX_ONLINE_USER_NUM_CH 20
#define MAX_FILE_NUM_CH 20
#define MAX_FILENAME_NUM_CH 256
#define MAX_SERVER_RESPONSE_CH_NUM 1024
#define MAX_PORT_CH_NUM 5
#define CHUNK_SIZE 1024
#define REQUEST_LOGIN_CHECK "REQUEST_LOGIN_CHECK"
#define REQUEST_NEW_ACCOUNT_CHECK "REQUEST_NEW_ACCOUNT_CHECK"
#define REQUEST_EXIT "REQUEST_EXIT"
#define REQUEST_LIST_SERVER_DIR "REQUEST_LIST_SERVER_DIR"
#define REQUEST_LIST_ONLINE_CLIENTS "REQUEST_LIST_ONLINE_CLIENTS"
#define REQUEST_SEND_FILE_TO_LOCAL "REQUEST_SEND_FILE_TO_LOCAL"
#define REQUEST_SEND_FILE_TO_CLIENT "REQUEST_SEND_FILE_TO_CLIENT"
#define REQUEST_CHECK_USER_ONLINE "REQUEST_CHECK_USER_ONLINE"
#define REQUEST_TEST_CONNECTION "REQUEST_TEST_CONNECTION"
#define REQUEST_CREATE_FORK "REQUEST_CREATE_FORK"
#define REQUEST_CREATE_THREAD_WATCHER "REQUEST_CREATE_THREAD_WATCHER"
#define REQUEST_CREATE_THREAD_DISPATCHER "REQUEST_CREATE_THREAD_DISPATCHER"
#define ACKNOWLEDGE "ACKNOWLEDGE"
#define SHARED_MEM_PERMISSIONS S_IRUSR | S_IWUSR
#define RESPONSE_ID_CORRECT "RESPONSE_ID_CORRECT"
#define RESPONSE_ID_INCORRECT "RESPONSE_ID_INCORRECT"
#define RESPONSE_CONNECTION_OKAY "RESPONSE_CONNECTION_OKAY"
#define CREATE_FILE O_WRONLY | O_CREAT
#define USER_PERMISSIONS S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH
#define FREE_SLOT "FREE"
#define END_PACKET "END_PACKET"
#define FTP_PACKET "FTP_PACKET_"
#define USER_ONLINE "USER_ONLINE"
#define USER_OFFLINE "USER_OFFLINE"
#define ID_INCORRECT 0
#define ID_CORRECT 1
#define LOGIN 2 
#define CREATE_NEW_ACCOUNT 3

typedef struct {
    int asyncTaskFD;
    char owner[MAX_CLIENT_ID_CH_NUM];
    struct sockaddr_in servAddr;
} asyncTaskInfo;

int iServerMainSocket,
    iCommunicationFD,
    iIdOnlineClientsNum,
    iIdOnlineClientsId,
    iClientLogged = 0,
    iSockOpt = 1;
int *ipOnlineClientsNum;
struct sockaddr_in servAddr;
struct ifaddrs *networkInterfaces;
char strClientRequestOrData[MAX_CLIENT_REQUEST_CH_NUM],
     strClientId[MAX_CLIENT_ID_CH_NUM],
     strSendedFilename[MAX_FILENAME_NUM_CH],
     strSendedClientId[MAX_CLIENT_ID_CH_NUM],
     strOnlineUserNum[MAX_ONLINE_USER_NUM_CH];
char (*cpOnlineClientIds)[MAX_ONLINE_USERS][MAX_CLIENT_ID_CH_NUM];
key_t key;
sem_t sharedSem;
ssize_t sendedData,
        receivedData;
pthread_t connectionWatcherId,
          fileDispatcherId;
pthread_attr_t threadsAttr;
asyncTaskInfo connectionWatcherInfo,
              fileDispatcherInfo;
struct timeval start, stop;

void selectNetworkInterface();
void printNetworkInterfaceInfo(struct ifaddrs *interface);
void printInfo();
void printOnlineClients();
int findFreeSlot();
int findClientSlot();
int checkClientId(int iWhy);
void listServerDirOfClient();
void listOnlineClients();
void receiveRequestOrDataFromClient(char *cpContainer, size_t size);
void sendResponseOrDataToClient(char *cpResponse);
void sendFileToServersLocal();
void sendFileToClient();
void newAccountCheck();
void loginCheck();
void checkUserOnline();
void startConnectionWatcher();
void startFileDispatcher();
void * asyncTaskConnectionWatcher(void *args);
void * asyncTaskFileDispatcher(void *args);
ssize_t safeWrite(int iFileDes, void *vpBuf, size_t size);

static void clientExited(int signo) {
	pid_t clientPid;
	int status;

	while((clientPid = waitpid(-1, &status, WNOHANG)) > 0);
}

static void serverTerminated(int signo) {
    if (shmdt(ipOnlineClientsNum) == -1)
        exit(1);
        
    if (shmdt(cpOnlineClientIds) == -1)
        exit(1);
        
    if (shmctl(iIdOnlineClientsNum, IPC_RMID, NULL) == -1)
        exit(1);
    
    if (shmctl(iIdOnlineClientsId, IPC_RMID, NULL) == -1)
        exit(1);
    
    if (sem_destroy(&sharedSem) == -1)
        exit(1);
    
    close(iServerMainSocket);
    exit(1);
}

int main(int argc, char *argv[]) {
    int i;
    pid_t childpid;
    
    gettimeofday(&start, NULL);
        
    if (argc != 2) {
        fprintf(stderr, "USAGE: %s <port number>\n", argv[0]);
        exit(1);
    } else {
        signal(SIGCHLD, clientExited);
        signal(SIGINT, serverTerminated);
        signal(SIGPIPE, SIG_IGN);
        
        if ((iServerMainSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            fprintf(stderr, ANSI_COLOR_RED "ERROR - Server main socket can't be created!\n" ANSI_COLOR_RESET);
            exit(1);
        }
        
        if (setsockopt(iServerMainSocket, SOL_SOCKET, SO_REUSEADDR, &iSockOpt, sizeof(iSockOpt)) == -1) {
            fprintf(stderr, ANSI_COLOR_RED "Error - Socket operation can't be applied!\n" ANSI_COLOR_RESET);
            exit(1); 
        }
          
         bzero(&servAddr, sizeof(servAddr));
         
         selectNetworkInterface();
         
         servAddr.sin_family = AF_INET;
         servAddr.sin_port = htons(atoi(argv[1]));
         
         if (bind(iServerMainSocket, (struct sockaddr *)&servAddr, sizeof(servAddr)) == -1) {
             fprintf(stderr, ANSI_COLOR_RED "ERROR - Server address and port can't be binded to socket!\n"
                ANSI_COLOR_RESET);
             exit(1);
         }
         
         if (listen(iServerMainSocket, 10) == -1) {
             fprintf(stderr, ANSI_COLOR_RED "ERROR - Socket can't be listened!\n" ANSI_COLOR_RESET);
             exit(1);
         }
        
        key = ftok(getcwd(NULL,0), 0);

        if ((iIdOnlineClientsNum = shmget(key, sizeof(int), SHARED_MEM_PERMISSIONS | IPC_CREAT | IPC_EXCL)) == -1) {
            fprintf(stderr, ANSI_COLOR_RED "ERROR - Couldn't get shared memory!\n" ANSI_COLOR_RESET);
            exit(1);
        }
        
        if ((ipOnlineClientsNum = (int *)shmat(iIdOnlineClientsNum, NULL, 0)) == (void *)-1) {
            fprintf(stderr, ANSI_COLOR_RED "ERROR - Shared memory can't be attached!\n" ANSI_COLOR_RESET);
            exit(1);
        }
        
        if ((iIdOnlineClientsId = shmget(IPC_PRIVATE, sizeof(char *), 
                                         SHARED_MEM_PERMISSIONS | IPC_CREAT | IPC_EXCL)) == -1) {
            fprintf(stderr, ANSI_COLOR_RED "ERROR - Couldn't get shared memory!\n" ANSI_COLOR_RESET);
            exit(1);                              
        }
        
        if ((cpOnlineClientIds = shmat(iIdOnlineClientsId, NULL, 0)) == (void *)-1) {
            fprintf(stderr, ANSI_COLOR_RED "ERROR - Shared memory can't be attached!\n" ANSI_COLOR_RESET);
            exit(1);
        }
                
        for (i = 0; i < MAX_ONLINE_USERS; ++i)
            strcpy((*cpOnlineClientIds)[i], FREE_SLOT);
          
        if (sem_init(&sharedSem, 0, 1) == -1) {
            fprintf(stderr, ANSI_COLOR_RED "ERROR - Semaphore can't be initialized!\n" ANSI_COLOR_RESET);
            exit(1);
        }
        
         gettimeofday(&stop, NULL); 
         printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
         printf("Server started running...\n");
         
         printf("\n------ FTP SERVER INFO ------\n");
         printf("IP Address: %s\n", inet_ntoa(servAddr.sin_addr));
         printf("Port number: %s\n", argv[1]);
                  
         *ipOnlineClientsNum = 0;

         pthread_attr_init(&threadsAttr);
         pthread_attr_setdetachstate(&threadsAttr, PTHREAD_CREATE_DETACHED);
         
         printInfo();
         
         for (;;) {
             memset(strClientRequestOrData, 0, sizeof(strClientRequestOrData));
             
             iCommunicationFD = accept(iServerMainSocket, (struct sockaddr *)NULL, NULL);
             
             if (iCommunicationFD == -1) {
                 fprintf(stderr, ANSI_COLOR_RED "ERROR - Request can't be accepted!\n" ANSI_COLOR_RESET);
                 exit(1);
             } else {
                 receiveRequestOrDataFromClient(strClientRequestOrData, sizeof(strClientRequestOrData));
                 
                 if (strcmp(strClientRequestOrData, REQUEST_CREATE_FORK) == 0) {
                    if ((childpid = fork()) == 0)
                        break;
                 } else if (strcmp(strClientRequestOrData, REQUEST_CREATE_THREAD_WATCHER) == 0) {
                     startConnectionWatcher();
                 } else if (strcmp(strClientRequestOrData, REQUEST_CREATE_THREAD_DISPATCHER) == 0) {
                    startFileDispatcher();
                 }
             }
         }
         
         if (childpid == 0) {             
             receiveRequestOrDataFromClient(strClientRequestOrData, sizeof(strClientRequestOrData));
             
             gettimeofday(&stop, NULL); 
             printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
             printf(ANSI_COLOR_YELLOW "INFO - Request of client: %s\n" ANSI_COLOR_RESET, strClientRequestOrData);
             
             while(strcmp(strClientRequestOrData, REQUEST_EXIT) != 0) {
                 if (strcmp(strClientRequestOrData, REQUEST_LIST_SERVER_DIR) == 0)
                    listServerDirOfClient();
                 else if (strcmp(strClientRequestOrData, REQUEST_LIST_ONLINE_CLIENTS) == 0)
                    listOnlineClients();
                 else if (strcmp(strClientRequestOrData, REQUEST_LOGIN_CHECK) == 0)
                    loginCheck();
                 else if (strcmp(strClientRequestOrData, REQUEST_NEW_ACCOUNT_CHECK) == 0)
                    newAccountCheck();
                 else if (strcmp(strClientRequestOrData, REQUEST_SEND_FILE_TO_LOCAL) == 0)
                    sendFileToServersLocal();
                 else if (strcmp(strClientRequestOrData, REQUEST_SEND_FILE_TO_CLIENT) == 0)
                    sendFileToClient();
                 else if (strcmp(strClientRequestOrData, REQUEST_CHECK_USER_ONLINE) == 0)
                    checkUserOnline();
                    
                if (strcmp(strClientRequestOrData, REQUEST_EXIT) != 0) {
                    printInfo();
                    receiveRequestOrDataFromClient(strClientRequestOrData, sizeof(strClientRequestOrData));
                    gettimeofday(&stop, NULL); 
                    printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
                    printf(ANSI_COLOR_YELLOW "INFO - Request of client: %s\n" ANSI_COLOR_RESET, 
                            strClientRequestOrData);
                }
            }
            
            if (iClientLogged) {
                gettimeofday(&stop, NULL); 
                printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, 
                    stop.tv_usec - start.tv_usec);
                printf("%s exiting from server...\n", strClientId);
                sem_wait(&sharedSem);
                --*ipOnlineClientsNum;
                strcpy((*cpOnlineClientIds)[findClientSlot()], FREE_SLOT);
                printOnlineClients();
                printInfo();
                sem_post(&sharedSem);
            } else {
                gettimeofday(&stop, NULL); 
                printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
                printf("Guest client exiting from server\n");
                printInfo();
            }
            exit(1);
         }
    }
    
    return 0;
}

void startConnectionWatcher() {
    memcpy(&(connectionWatcherInfo.servAddr), &servAddr, sizeof(servAddr));
    connectionWatcherInfo.asyncTaskFD = iCommunicationFD;
    pthread_create(&connectionWatcherId, &threadsAttr, asyncTaskConnectionWatcher,
         (void *)&connectionWatcherInfo);   
}

void startFileDispatcher() {
   memcpy(&(fileDispatcherInfo.servAddr), &servAddr, sizeof(servAddr));                  
   fileDispatcherInfo.asyncTaskFD = iCommunicationFD; 
   receiveRequestOrDataFromClient(strClientId, sizeof(strClientId)); 
   strcpy(fileDispatcherInfo.owner, strClientId); 
   pthread_create(&fileDispatcherId, &threadsAttr, asyncTaskFileDispatcher,
     (void *)&fileDispatcherInfo);  
}

void * asyncTaskConnectionWatcher(void * args) {
    char strTestRequest[MAX_CLIENT_REQUEST_CH_NUM];
    asyncTaskInfo *watcherInfo;
    
    watcherInfo = (asyncTaskInfo *)args;
    
    while (errno != EPIPE) {
        recv(watcherInfo->asyncTaskFD, strTestRequest, sizeof(strTestRequest), 0);
        send(watcherInfo->asyncTaskFD, RESPONSE_CONNECTION_OKAY, strlen(RESPONSE_CONNECTION_OKAY), 0);
    }
    
    pthread_exit((void *)NULL);
}

void * asyncTaskFileDispatcher(void * args) {
    DIR *dir;
    char strDispatcherRequest[MAX_CLIENT_REQUEST_CH_NUM],
         path[PATH_MAX],
         filename[MAX_FILENAME_NUM_CH],
         originFilename[MAX_FILENAME_NUM_CH],
         cpyFilename[MAX_FILENAME_NUM_CH],
         foundedFile[MAX_FILENAME_NUM_CH],
         fileContentChunk[CHUNK_SIZE],
         *token;
    struct dirent *sdirpEntry;
    int iPacketArrived = 0,
        iFileFD;
    
    asyncTaskInfo *dispatcherInfo;
    
    dispatcherInfo = (asyncTaskInfo *)args;
    
    strcpy(path, getcwd(NULL, 0));
    sprintf(path, "%s/%s", path, dispatcherInfo->owner);
    
    if ((dir = opendir(path)) == NULL)
       pthread_exit((void *)NULL);  
       
    while (errno != EPIPE) {
        while (!iPacketArrived && errno != EPIPE) {
            while ((sdirpEntry = readdir(dir)) != NULL) {
                if (strncmp(sdirpEntry->d_name, FTP_PACKET, strlen(FTP_PACKET)) == 0) {
                    iPacketArrived = 1;
                    strcpy(filename, sdirpEntry->d_name);
                    strcpy(cpyFilename, sdirpEntry->d_name);
                }
            }
            
            rewinddir(dir);
        }
        
        sprintf(foundedFile, "%s/%s", path, filename);
        
        strtok(cpyFilename, "_");
        strtok(NULL, "_");
        strcpy(originFilename, strtok(NULL, "_"));
        
        
        send(dispatcherInfo->asyncTaskFD, originFilename, strlen(originFilename), 0);
        recv(dispatcherInfo->asyncTaskFD, strDispatcherRequest, sizeof(strDispatcherRequest), 0);
        
        iFileFD = open(foundedFile, O_RDONLY);
        
        while (read(iFileFD, fileContentChunk, sizeof(fileContentChunk)) != 0) {
            write(dispatcherInfo->asyncTaskFD, fileContentChunk, sizeof(fileContentChunk));
            
            read(dispatcherInfo->asyncTaskFD, strDispatcherRequest, sizeof(strDispatcherRequest));
                
            memset(fileContentChunk, 0, sizeof(fileContentChunk));
            memset(strDispatcherRequest, 0, sizeof(strDispatcherRequest));
	    }
	
        write(dispatcherInfo->asyncTaskFD, END_PACKET, strlen(END_PACKET));
        
        read(dispatcherInfo->asyncTaskFD, strDispatcherRequest, sizeof(strDispatcherRequest));
 
        close(iFileFD);
        
        unlink(foundedFile);  
        iPacketArrived = 0;
        rewinddir(dir);    
    }
    
    pthread_exit((void *)NULL);
}

// Admin choices network interfaces which they are in this computer
void selectNetworkInterface() {
    int iAdminSelectedInterface = 0;
    char chAdminAnswer;
    struct ifaddrs *interface;
    
    getifaddrs(&networkInterfaces);
    
    for (interface = networkInterfaces; interface != NULL; interface = interface->ifa_next) {
        if (interface->ifa_addr->sa_family == AF_INET) {
            printNetworkInterfaceInfo(interface);
            
            printf("Your machine have connected to this interface ? [Y/N] (Q: Exit): ");
            scanf(" %c", &chAdminAnswer);
            
            while (chAdminAnswer != 'Y' && chAdminAnswer != 'y' &&
                   chAdminAnswer != 'N' && chAdminAnswer != 'n' &&
                   chAdminAnswer != 'Q' && chAdminAnswer != 'q') {
                system("clear");
                printNetworkInterfaceInfo(interface);
                printf("ERROR - Wrong Input! Please enter again! :)\n");
                printf("Your machine have connected to this interface ? [Y/N] (Q: Exit): ");
                scanf(" %c", &chAdminAnswer);
            }
            
            if (chAdminAnswer == 'Y' || chAdminAnswer == 'y') {
                system("clear");
                servAddr.sin_addr = ((struct sockaddr_in *)interface->ifa_addr)->sin_addr;
                iAdminSelectedInterface = 1;
                break;
            } else if (chAdminAnswer == 'N' || chAdminAnswer == 'n') {
                system("clear");
            } else if (chAdminAnswer == 'Q' || chAdminAnswer == 'q') {
                system("clear");
                printf("Exiting from server...\n");
                exit(1);
            }
        }
    }
    
    if (!iAdminSelectedInterface) {
        fprintf(stderr, "Server admin hadn't selected any interface! Quitting...\n");
        exit(1);
    }
}

void printNetworkInterfaceInfo(struct ifaddrs *interface) {
    printf("--------- FTP SERVER CONFIGURATION ---------\n");
    printf("Interface: %s\n", interface->ifa_name);
    printf("IP Address: %s\n", inet_ntoa(((struct sockaddr_in *)interface->ifa_addr)->sin_addr));
}

int checkClientId(int iWhy) {
    DIR *dir;
    struct dirent *sdirpEntry;
    int resultCheck,
        resultLoginCheck = ID_INCORRECT,
        resultCreateNewAccountCheck = ID_CORRECT;
    
    if (!(dir = opendir(getcwd(NULL, 0))))
        return;
    
     while ((sdirpEntry = readdir(dir)) != NULL) {
        if (sdirpEntry->d_type == DT_DIR &&
            strcmp(sdirpEntry->d_name, strClientId) == 0) {
            if (iWhy == CREATE_NEW_ACCOUNT) {
                resultCreateNewAccountCheck = ID_INCORRECT;
            } else if (iWhy == LOGIN) {
                resultLoginCheck = ID_CORRECT;
            }
            break;
        } 
     }
    
    closedir(dir);
    
    if (iWhy == CREATE_NEW_ACCOUNT)
        resultCheck = resultCreateNewAccountCheck;
    else
        resultCheck = resultLoginCheck;
    
    gettimeofday(&stop, NULL); 
    printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
    printf(ANSI_COLOR_YELLOW "INFO - Check result: %s\n" ANSI_COLOR_RESET, 
        resultCheck == ID_CORRECT ? "Client id is correct ✓" : "Client id is incorrect ✗");
    
    return resultCheck;
}

int registerClientToServer() {
   mkdir(strClientId, 0777);
}

void listServerDirOfClient() {
   DIR *dir;
   struct dirent *sdirpEntry;
   int iRequiredChNum, iFileNum = 0;
   char *dirPath,
        strFileNum[MAX_FILE_NUM_CH];
   
   iRequiredChNum = snprintf(NULL, 0, "%s/%s", getcwd(NULL, 0), strClientId);
   dirPath = (char *)calloc(iRequiredChNum + 1, sizeof(char));
   sprintf(dirPath, "%s/%s", getcwd(NULL, 0), strClientId);
  
   
   if (!(dir = opendir(dirPath)))
      return;
   
   while ((sdirpEntry = readdir(dir)) != NULL)
     if (strcmp(sdirpEntry->d_name, ".") != 0 && strcmp(sdirpEntry->d_name, "..") != 0)
        ++iFileNum;
   
   sprintf(strFileNum, "%d", iFileNum);    
   
   rewinddir(dir);
   
   usleep(500);
   sendResponseOrDataToClient(strFileNum);
   
   while ((sdirpEntry = readdir(dir)) != NULL)
       if (strcmp(sdirpEntry->d_name, ".") != 0 && strcmp(sdirpEntry->d_name, "..") != 0) {
            sendResponseOrDataToClient(sdirpEntry->d_name);
            sendResponseOrDataToClient(sdirpEntry->d_type == DT_DIR ? "Directory" : "Regular File");
       }
   
   closedir(dir);
   free(dirPath);
}

void newAccountCheck() {
    do {
        receiveRequestOrDataFromClient(strClientId, sizeof(strClientId));
        
        gettimeofday(&stop, NULL); 
        printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
        printf(ANSI_COLOR_YELLOW "INFO - Checking new account information ...\n" 
                ANSI_COLOR_RESET);
        
        if (checkClientId(CREATE_NEW_ACCOUNT)) {
            sendResponseOrDataToClient(RESPONSE_ID_CORRECT);
            registerClientToServer(); // register client to server
            iClientLogged = 1;
            
            sem_wait(&sharedSem);
            ++*ipOnlineClientsNum;
            strcpy((*cpOnlineClientIds)[findFreeSlot()], strClientId);
            printOnlineClients();
            sem_post(&sharedSem);
            break;
        }
        
        sendResponseOrDataToClient(RESPONSE_ID_INCORRECT);
        
        printInfo();
        
        receiveRequestOrDataFromClient(strClientRequestOrData, sizeof(strClientRequestOrData));
    } while (strcmp(strClientRequestOrData, REQUEST_NEW_ACCOUNT_CHECK) == 0);
}

void loginCheck() {
     do {
        receiveRequestOrDataFromClient(strClientId, sizeof(strClientId)); // take client id from client
    
        gettimeofday(&stop, NULL); 
        printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
        printf(ANSI_COLOR_YELLOW "INFO - Checking login information...\n" ANSI_COLOR_RESET);
    
        if (checkClientId(LOGIN)) {
            sendResponseOrDataToClient(RESPONSE_ID_CORRECT);
            iClientLogged = 1;
            
            sem_wait(&sharedSem);
            ++*ipOnlineClientsNum;
            strcpy((*cpOnlineClientIds)[findFreeSlot()], strClientId);
            printOnlineClients();
            sem_post(&sharedSem);
            break;
        }    
        
        sendResponseOrDataToClient(RESPONSE_ID_INCORRECT);
        
        printInfo();
        
        receiveRequestOrDataFromClient(strClientRequestOrData, sizeof(strClientRequestOrData));
    } while (strcmp(strClientRequestOrData, REQUEST_LOGIN_CHECK) == 0);
}

void listOnlineClients() {
    int i, j = 0;
    
    sprintf(strOnlineUserNum, "%d", *ipOnlineClientsNum);
   
    usleep(500);
    sendResponseOrDataToClient(strOnlineUserNum);
    
    for (i = 0; i < MAX_ONLINE_USERS; ++i)
        if (strcmp((*cpOnlineClientIds)[i], FREE_SLOT) != 0 && j < *ipOnlineClientsNum) {
            sendResponseOrDataToClient((*cpOnlineClientIds)[i]);
            ++j;
        }
}

void sendFileToServersLocal() {
    int iFileFD;
    char fileContentChunk[CHUNK_SIZE];
    
    receiveRequestOrDataFromClient(strSendedFilename, sizeof(strSendedFilename));
    
    gettimeofday(&stop, NULL); 
    printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
    printf(ANSI_COLOR_YELLOW "INFO - Filename of file will be sended: %s\n" ANSI_COLOR_RESET,
        strSendedFilename);
    
    chdir(strClientId);
    
    iFileFD = open(strSendedFilename, CREATE_FILE, USER_PERMISSIONS);
    
    do {
        memset(fileContentChunk, 0, sizeof(fileContentChunk));
        
        read(iCommunicationFD, fileContentChunk, sizeof(fileContentChunk));
     
        write(iCommunicationFD, ACKNOWLEDGE, sizeof(ACKNOWLEDGE));
        
        if (strcmp(fileContentChunk, END_PACKET) != 0)
           write(iFileFD, fileContentChunk, sizeof(fileContentChunk));
    } while (strcmp(fileContentChunk, END_PACKET) != 0);
    
    close(iFileFD);
    
    chdir("..");
}

void sendFileToClient() {
    int iFileFD;
    char fileContentChunk[CHUNK_SIZE],
         strFtpPacket[MAX_FILENAME_NUM_CH];
    
    receiveRequestOrDataFromClient(strSendedFilename, sizeof(strSendedFilename));
    
    gettimeofday(&stop, NULL); 
    printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
    printf(ANSI_COLOR_YELLOW "INFO - Filename of file will be sended: %s\n" ANSI_COLOR_RESET,
        strSendedFilename);
        
    receiveRequestOrDataFromClient(strSendedClientId, sizeof(strSendedClientId));
    
    gettimeofday(&stop, NULL); 
    printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
    printf(ANSI_COLOR_YELLOW "INFO - Sended client id: %s\n" ANSI_COLOR_RESET, strSendedClientId);
    
    strcpy(strFtpPacket, FTP_PACKET);
    strcat(strFtpPacket, strSendedFilename);
    
    chdir(strSendedClientId);
    
    iFileFD = open(strFtpPacket, CREATE_FILE, USER_PERMISSIONS);
    
    do {
        memset(fileContentChunk, 0, sizeof(fileContentChunk));
        
        read(iCommunicationFD, fileContentChunk, sizeof(fileContentChunk));
        
        write(iCommunicationFD, ACKNOWLEDGE, sizeof(ACKNOWLEDGE));
       
        if (strcmp(fileContentChunk, END_PACKET) != 0)
           write(iFileFD, fileContentChunk, sizeof(fileContentChunk));
    } while (strcmp(fileContentChunk, END_PACKET) != 0);
    
    close(iFileFD);
    
    chdir("..");
}

void checkUserOnline() {
    int i,
        iUserOnline = 0;
    
    receiveRequestOrDataFromClient(strClientRequestOrData, sizeof(strClientRequestOrData));
    
    for (i = 0; i < *ipOnlineClientsNum; ++i) {
        if (strcmp(strClientRequestOrData, (*cpOnlineClientIds)[i]) == 0)
           iUserOnline = 1;
    }
    
    if (iUserOnline)
        sendResponseOrDataToClient(USER_ONLINE);
    else 
        sendResponseOrDataToClient(USER_OFFLINE);
}

// Receives request from client and send acknowledge info to client
void receiveRequestOrDataFromClient(char *cpContainer, size_t size) {
    // Receive request from server
    receivedData = recv(iCommunicationFD, cpContainer, size, 0);
    cpContainer[receivedData] = '\0';

    // Send ACKNOWLEDGE to client
    sendedData = send(iCommunicationFD, ACKNOWLEDGE, strlen(ACKNOWLEDGE), 0);
}

// Send response to client and takes acknowledge info from client
void sendResponseOrDataToClient(char *cpResponse) {
    sendedData = send(iCommunicationFD, cpResponse, strlen(cpResponse), 0);
    
    // Receive ACKNOWLEDGE from client
    receivedData = recv(iCommunicationFD, strClientRequestOrData, sizeof(strClientRequestOrData), 0);
    strClientRequestOrData[receivedData] = '\0';
}

void printInfo() {
    gettimeofday(&stop, NULL); 
    printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
    printf(ANSI_COLOR_YELLOW "INFO - Online clients number in server: %d\n"
         ANSI_COLOR_RESET, *ipOnlineClientsNum);
    printf("\nServer awaiting requests...\n");  
}

int findFreeSlot() {
    int i,
        iFreeSlotLoc;
    
    for (i = 0; i < MAX_ONLINE_USERS; ++i) {
        if (strcmp((*cpOnlineClientIds)[i], FREE_SLOT) == 0) {
            iFreeSlotLoc = i;
            break;
        }
    }   
    
    return iFreeSlotLoc;
}

int findClientSlot() {
    int i,
        iClientSlotLoc;
        
    for (i = 0; i < MAX_ONLINE_USERS; ++i) {
        if (strcmp((*cpOnlineClientIds)[i], strClientId) == 0) {
            iClientSlotLoc = i;
            break;
        }
    }
    
    return iClientSlotLoc;
}

void printOnlineClients() {
    int i,
        iShowedNumber = 0;
    
     gettimeofday(&stop, NULL); 
     printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
     printf("\n----- ONLINE CLIENTS ----\n");
     for (i = 0; i < MAX_ONLINE_USERS; ++i)
         if (strcmp((*cpOnlineClientIds)[i], FREE_SLOT) != 0 && iShowedNumber < *ipOnlineClientsNum) {
            printf("Client Id: %s\n", (*cpOnlineClientIds)[i]);
            ++iShowedNumber;
         }
     
     if (*ipOnlineClientsNum == 0)
        printf("No clients online in server!\n");
     printf("\n");
}