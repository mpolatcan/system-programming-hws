/* CSE 244 - System Programming 
   Final Project - FTP Server
   Mutlu POLATCAN - 121044062 */
   
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <pthread.h>

#define ANSI_COLOR_RED     "\x1b[1;31m"
#define ANSI_COLOR_GREEN   "\x1b[1;32m"
#define ANSI_COLOR_YELLOW  "\x1b[1;33m"
#define ANSI_COLOR_MAGENTA "\x1b[1;35m"
#define ANSI_COLOR_CYAN    "\x1b[1;36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define MAX_CLIENT_ID_CH_NUM 64
#define MAX_SERVER_RESPONSE_CH_NUM 1024
#define MAX_CLIENT_COMMAND 512
#define MAX_ONLINE_USERS 100
#define MAX_INTEGER_CH_NUM 20
#define MAX_FILE_NAME_CH_NUM 256
#define MAX_TYPE_CH_NUM 13
#define CHUNK_SIZE 1024
#define REQUEST_LOGIN_CHECK "REQUEST_LOGIN_CHECK"
#define REQUEST_NEW_ACCOUNT_CHECK "REQUEST_NEW_ACCOUNT_CHECK"
#define REQUEST_LIST_ONLINE_CLIENTS "REQUEST_LIST_ONLINE_CLIENTS"
#define REQUEST_EXIT "REQUEST_EXIT"
#define REQUEST_LIST_SERVER_DIR "REQUEST_LIST_SERVER_DIR"
#define REQUEST_SEND_FILE_TO_LOCAL "REQUEST_SEND_FILE_TO_LOCAL"
#define REQUEST_SEND_FILE_TO_CLIENT "REQUEST_SEND_FILE_TO_CLIENT"
#define REQUEST_CHECK_USER_ONLINE "REQUEST_CHECK_USER_ONLINE"
#define REQUEST_TEST_CONNECTION "REQUEST_TEST_CONNECTION"
#define REQUEST_CREATE_FORK "REQUEST_CREATE_FORK"
#define REQUEST_CREATE_THREAD_WATCHER "REQUEST_CREATE_THREAD_WATCHER"
#define REQUEST_CREATE_THREAD_DISPATCHER "REQUEST_CREATE_THREAD_DISPATCHER"
#define RESPONSE_ID_INCORRECT "RESPONSE_ID_INCORRECT"
#define CREATE_FILE O_WRONLY | O_CREAT
#define USER_PERMISSIONS S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH
#define ACKNOWLEDGE "ACKNOWLEDGE"
#define END_PACKET "END_PACKET"
#define USER_ONLINE "USER_ONLINE"
#define REQUEST 1
#define DATA 0

typedef struct {
    struct sockaddr_in servAddr;
} asyncTaskInfo;

int iCommunicationSocket,
	iOnlineUsersNum,
	iFileNumInServer;
ssize_t receivedData,
		sendedData;
struct sockaddr_in servAddr;
char strClientId[MAX_CLIENT_ID_CH_NUM],
	 strServerResponse[MAX_SERVER_RESPONSE_CH_NUM],
	 strClientCommand[MAX_CLIENT_COMMAND],
	 strSendedFilename[MAX_FILE_NAME_CH_NUM],
	 strSendedClientId[MAX_CLIENT_ID_CH_NUM];
char onlineClients[MAX_ONLINE_USERS][MAX_CLIENT_ID_CH_NUM];
char *cpNewline,
	 *cpToken,
	 **filesInServer,
	 **typesOfFilesInServer;
asyncTaskInfo connectionWatcherInfo,
			  fileDispatcherInfo;
pthread_t connectionWatcherId,
		  fileDispatcherId;
pthread_attr_t threadsAttr;
struct timeval start, stop;
			  
void serverLoginIntro();
void sendDataOrRequestToServer(char *cpRequest);
void receiveDataOrResponseFromServer();
void printCommandList();
void fileExplorer(char *cpCurDirPath);
void fileExplorerForServer();
void listOnlineClients();
void sendFileToServersLocal(char *cpSendedFile);
void sendFileToClient(char *cpSendedFile, char *cpSendedClient);
void sendFile(char *searchedFile, char *clientId);
int checkRequestOrData();
void startConnectionWatcher();
void startFileDispatcher();
void * asyncTaskConnectionWatcher(void *args);
void * asyncTaskFileDispatcher(void *args);
ssize_t safeRead(int iFileDes, void *vpBuf, size_t size);

static void clientTerminatedByUser(int argno) {
	sendDataOrRequestToServer(REQUEST_EXIT);
	printf("\nExiting from server...\n");
	close(iCommunicationSocket);
	exit(1);
}

static void clientTerminatedByServer(int argno) {
	printf("\nServer shutdowns...\n");
	close(iCommunicationSocket);
	exit(1);
}

int main(int argc, char *argv[])
{
	gettimeofday(&start, NULL);
	
	if (argc != 3) {
		fprintf(stderr, "USAGE: %s <ip address> <port number>\n", argv[0]);
		exit(1);
	} else {
		signal(SIGINT, clientTerminatedByUser);
		signal(SIGPIPE, clientTerminatedByServer);
		
		if ((iCommunicationSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
			fprintf(stderr, "ERROR - Communication socket can't be created\n");
			exit(1);
		}

		bzero(&servAddr, sizeof(servAddr)); // clear memory address of sockaddr_in struct

		servAddr.sin_addr.s_addr = inet_addr(argv[1]);
		servAddr.sin_family = AF_INET;
		servAddr.sin_port = htons(atoi(argv[2]));
		
		if (connect(iCommunicationSocket, (struct sockaddr *)&servAddr, sizeof(servAddr)) == -1) {
			fprintf(stderr, "ERROR - Can't connected to FTP server!\n");
			exit(1);
		}
		
		sendDataOrRequestToServer(REQUEST_CREATE_FORK);
		
		memset(strClientId, 0, sizeof(strClientId)); // clear strClientId
		memset(strServerResponse, 0, sizeof(strServerResponse)); // clear strServerResponse
				
		pthread_attr_init(&threadsAttr);
		pthread_attr_setdetachstate(&threadsAttr, PTHREAD_CREATE_DETACHED);
		
		startConnectionWatcher();
		
		serverLoginIntro();
			
		startFileDispatcher();
		
		system("clear");
		
		gettimeofday(&stop, NULL);
		printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET,
			stop.tv_usec - start.tv_usec);
		printf("Login successfull! Welcome client %s\n" ANSI_COLOR_RESET, strClientId);
					
		for (;;) {
			printf("\n------- FTP SERVER TERMINAL -------\n");
			printf(ANSI_COLOR_YELLOW "INFO - If you want to see available comments enter \"help\"\n"
				ANSI_COLOR_RESET);
			printf("\nPlease enter corresponding command (CTRL-C: Exit) > ");
			fgets(strClientCommand, MAX_CLIENT_COMMAND, stdin);
			cpNewline = strchr(strClientCommand, '\n');
			*cpNewline = '\0';
					
			system("clear");
			if (strcmp(strClientCommand, "help") == 0)
				printCommandList();
			else if (strcmp(strClientCommand, "listLocal") == 0) {
				fileExplorer(getcwd(NULL, 0));
			} else if (strcmp(strClientCommand, "lsClients") == 0) {
				listOnlineClients();
			} else if (strcmp(strClientCommand, "listServer") == 0) {
				fileExplorerForServer();
			} else if (strncmp(strClientCommand, "sendFile", 8) == 0) {
				strtok(strClientCommand, " ");
				cpToken = strtok(NULL, " ");
				
				if (cpToken != NULL)
					strcpy(strSendedFilename, cpToken);

				cpToken = strtok(NULL, " ");
				
				if (cpToken != NULL)
					strcpy(strSendedClientId, cpToken);
					
				sendFile(strSendedFilename, strSendedClientId);
				
				memset(strSendedFilename, 0, sizeof(strSendedFilename));
				memset(strSendedFilename, 0, sizeof(strSendedClientId));
			} else {
				printf(ANSI_COLOR_RED "\nINFO - WRONG COMMAND! Please enter again :)\n" ANSI_COLOR_RESET);
			}
		}
	}
	
	return 0;
}

void serverLoginIntro() {
	char chClientAnswer;
	
	printf("------ WELCOME TO FTP USER ------\n");
	printf("Do you have an account ? [Y/N] (Q / CTRL-C: Exit) > ");
	scanf(" %c", &chClientAnswer);
	
	while (chClientAnswer != 'Y' && chClientAnswer != 'y' &&
		   chClientAnswer != 'N' && chClientAnswer != 'n' &&
		   chClientAnswer != 'Q' && chClientAnswer != 'q') {
		system("clear");
		printf("------ WELCOME TO FTP USER ------\n");
		printf("ERROR - Wrong Input! Please enter again :)\n");
		printf("Do you have an account ? [Y/N] (Q / CTRL-C: Exit) > ");
		scanf(" %c", &chClientAnswer);
	}	

	if (chClientAnswer == 'Y' || chClientAnswer == 'y') {
		system("clear");
		
		printf("----- LOGIN TO SERVER -----\n");
		printf("Enter client id (max 64 characters) (CTRL-C: Exit) > ");
		scanf("%s",strClientId);
		
		sendDataOrRequestToServer(REQUEST_LOGIN_CHECK);
		
		sendDataOrRequestToServer(strClientId);
		
		receiveDataOrResponseFromServer();
		
		// While account informations is incorrect try again enter client id until find correct
		// client id		
		while (strcmp(strServerResponse, RESPONSE_ID_INCORRECT) == 0) {
			system("clear");
			printf("----- LOGIN TO SERVER -----\n");
			printf("ERROR - Any client not found with that client id! Please enter client id again :)\n");
			
			// clean buffers
			memset(strServerResponse, 0, sizeof(strServerResponse)); 
			memset(strClientId, 0, sizeof(strClientId));
				
			printf("Enter client id (max 64 characters) (CTRL-C: Exit) > ");
			scanf("%s", strClientId);
			
			sendDataOrRequestToServer(REQUEST_LOGIN_CHECK);
			
			sendDataOrRequestToServer(strClientId);
			
			receiveDataOrResponseFromServer();
		}
	} else if (chClientAnswer == 'N' || chClientAnswer == 'n') {
		system("clear");
		
		printf("Are you want to create an account? [Y/N] (N / CTRL-C: Exit) > ");
		scanf(" %c", &chClientAnswer);
		
		while (chClientAnswer != 'Y' && chClientAnswer != 'y' &&
		   chClientAnswer != 'N' && chClientAnswer != 'n') {
		   system("clear");
		   printf("ERROR - Wrong Input! Please enter again :)\n");
		   printf("Are you want to create an account? [Y/N] (N / CTRL-C: Exit) > ");
		   scanf(" %c", &chClientAnswer);
		}
		
		if (chClientAnswer == 'Y' || chClientAnswer == 'y') {
			printf("----- CREATE AN ACCOUNT -----\n");
			printf("Enter client id (max 64 characters) (CTRL-C: Exit) > ");
			scanf("%s",strClientId);
			
			sendDataOrRequestToServer(REQUEST_NEW_ACCOUNT_CHECK);
			
			sendDataOrRequestToServer(strClientId);
			
			receiveDataOrResponseFromServer();
				
			// While account informations is incorrect try again enter client id until find correct
			// client id
			while (strcmp(strServerResponse, RESPONSE_ID_INCORRECT) == 0) {
				system("clear");
				printf("----- CREATE AN ACCOUNT -----\n");
				printf("ERROR - These client id is not available! Please enter client id again :)\n");
				
				// clean buffers
				memset(strServerResponse, 0, sizeof(strServerResponse)); 
				memset(strClientId, 0, sizeof(strClientId)); 
				
				printf("Enter client id (max 64 characters) (CTRL-C: Exit) > ");
				scanf("%s",strClientId);
				
				sendDataOrRequestToServer(REQUEST_NEW_ACCOUNT_CHECK);
				
				sendDataOrRequestToServer(strClientId);
				
				receiveDataOrResponseFromServer();
				
				printf(ANSI_COLOR_YELLOW "\nINFO -> Check completed by server -> Response: %s\n" ANSI_COLOR_RESET, 
					strServerResponse);
			}
		} else if (chClientAnswer == 'N' || chClientAnswer == 'n') {
			sendDataOrRequestToServer(REQUEST_EXIT);
			printf("\nExiting from server...\n");
			exit(1);
		}
	} else if (chClientAnswer == 'Q' || chClientAnswer == 'q') {
		sendDataOrRequestToServer(REQUEST_EXIT);
		printf("\nExiting from server...\n");
		exit(1);
	}
	
	getchar();
}

void fileExplorer(char *cpCurDirPath) {
    DIR *dir;
    struct dirent *sdirpEntry;

    if (!(dir = opendir(cpCurDirPath)))
        return;

    printf("\n--------- FILE EXPLORER ---------\n");
    /* Counts elements number in current directory */
    while ((sdirpEntry = readdir(dir)) != NULL) {
        if (sdirpEntry->d_type == DT_REG) {
		   printf(ANSI_COLOR_GREEN "File name: " ANSI_COLOR_RESET);
           printf(ANSI_COLOR_CYAN "%s" ANSI_COLOR_RESET, sdirpEntry->d_name);
           printf(ANSI_COLOR_RED " ->" ANSI_COLOR_RESET);
           printf(ANSI_COLOR_YELLOW " Type of file: Regular file\n" ANSI_COLOR_RESET);
        } else if (sdirpEntry->d_type == DT_DIR && strcmp(sdirpEntry->d_name, ".") != 0 &&
              strcmp(sdirpEntry->d_name, "..") != 0) {
		  printf(ANSI_COLOR_GREEN "File name: " ANSI_COLOR_RESET);		  
          printf(ANSI_COLOR_CYAN "%s" ANSI_COLOR_RESET, sdirpEntry->d_name);
          printf(ANSI_COLOR_RED " ->" ANSI_COLOR_RESET);
          printf(ANSI_COLOR_YELLOW " Type of file: Directory\n" ANSI_COLOR_RESET);        
        }
    }
    
    closedir(dir);    
}

void startConnectionWatcher() {
	memcpy(&(connectionWatcherInfo.servAddr),&servAddr,sizeof(servAddr));
	pthread_create(&connectionWatcherId, &threadsAttr, asyncTaskConnectionWatcher, 
		(void *)&connectionWatcherInfo);	
}

void startFileDispatcher() {
	memcpy(&(fileDispatcherInfo.servAddr), &servAddr, sizeof(servAddr));
	pthread_create(&fileDispatcherId, &threadsAttr, asyncTaskFileDispatcher, (void *)&fileDispatcherInfo);
}

void * asyncTaskConnectionWatcher(void *args) {
	int iConnectionCheckerSocket;
	char strServerTestResponse[MAX_SERVER_RESPONSE_CH_NUM];
	asyncTaskInfo *connectInfo;
	
	connectInfo = (asyncTaskInfo *)args;

	if ((iConnectionCheckerSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, ANSI_COLOR_RED "ERROR WATCHER - Connection checker socket can't be created\n" ANSI_COLOR_RESET);
		exit(1);
	}
	
	if (connect(iConnectionCheckerSocket, (struct sockaddr *)&(connectInfo->servAddr), sizeof(connectInfo->servAddr)) == -1) {
		fprintf(stderr, ANSI_COLOR_RED "ERROR WATCHER - Connection checker can't connected to FTP server!\n" ANSI_COLOR_RESET);
		exit(1);
	}
	
	send(iConnectionCheckerSocket, REQUEST_CREATE_THREAD_WATCHER, strlen(REQUEST_CREATE_THREAD_WATCHER), 0);
	recv(iConnectionCheckerSocket, strServerTestResponse, sizeof(strServerTestResponse), 0);
	
    for(;;) {
		send(iConnectionCheckerSocket, REQUEST_TEST_CONNECTION, strlen(REQUEST_TEST_CONNECTION), 0);
		recv(iConnectionCheckerSocket, strServerTestResponse, sizeof(strServerTestResponse), 0);
	}
}

void * asyncTaskFileDispatcher(void *args) {
	int iFileDispatcherSocket,
		iFileFD;
	char strServerDispatcherRequestOrResponse[MAX_SERVER_RESPONSE_CH_NUM],
	     incomingFilename[MAX_FILE_NAME_CH_NUM],
		 fileContentChunk[CHUNK_SIZE];
	asyncTaskInfo *dispatcherInfo;
	
	dispatcherInfo = (asyncTaskInfo *)args;
	
	if ((iFileDispatcherSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, ANSI_COLOR_RED "ERROR WATCHER - Connection checker socket can't be created\n" ANSI_COLOR_RESET);
		exit(1);
	}
	
	if (connect(iFileDispatcherSocket, (struct sockaddr *)&(dispatcherInfo->servAddr), 
		sizeof(dispatcherInfo->servAddr)) == -1) {
		fprintf(stderr, ANSI_COLOR_RED "ERROR WATCHER - Connection checker can't connected to FTP server!\n" ANSI_COLOR_RESET);
		exit(1);
	}
	
	memset(strServerDispatcherRequestOrResponse, 0, sizeof(strServerDispatcherRequestOrResponse));
	
	send(iFileDispatcherSocket, REQUEST_CREATE_THREAD_DISPATCHER,
		strlen(REQUEST_CREATE_THREAD_DISPATCHER), 0);
	recv(iFileDispatcherSocket, strServerDispatcherRequestOrResponse, 
		sizeof(strServerDispatcherRequestOrResponse), 0);
	
	memset(strServerDispatcherRequestOrResponse, 0, sizeof(strServerDispatcherRequestOrResponse));
	
	send(iFileDispatcherSocket, strClientId, strlen(strClientId), 0);
	recv(iFileDispatcherSocket, strServerDispatcherRequestOrResponse,
		sizeof(strServerDispatcherRequestOrResponse), 0);
	
	memset(strServerDispatcherRequestOrResponse, 0, sizeof(strServerDispatcherRequestOrResponse));
	
	for (;;) {
		recv(iFileDispatcherSocket, incomingFilename, sizeof(incomingFilename), 0);
		printf(ANSI_COLOR_YELLOW "\nINFO - File incoming... -> %s\n" ANSI_COLOR_RESET, incomingFilename);
		send(iFileDispatcherSocket, ACKNOWLEDGE, strlen(ACKNOWLEDGE), 0);
		printf("> ");
		iFileFD = open(incomingFilename, CREATE_FILE, USER_PERMISSIONS);
		
		do {
			memset(fileContentChunk, 0, sizeof(fileContentChunk));
			
			read(iFileDispatcherSocket, fileContentChunk, sizeof(fileContentChunk));
			
			write(iFileDispatcherSocket, ACKNOWLEDGE, sizeof(ACKNOWLEDGE));
			
			if (strcmp(fileContentChunk, END_PACKET) != 0)
				write(iFileFD, fileContentChunk, sizeof(fileContentChunk));
		} while (strcmp(fileContentChunk, END_PACKET) != 0);
		
		close(iFileFD);
		
		memset(incomingFilename, 0, sizeof(incomingFilename));
	}
}

void fileExplorerForServer() {
	int i;
	
	sendDataOrRequestToServer(REQUEST_LIST_SERVER_DIR);
				
	receiveDataOrResponseFromServer();
	
	iFileNumInServer = atoi(strServerResponse);
	
	filesInServer = (char **)calloc(iFileNumInServer, sizeof(char *));
	typesOfFilesInServer = (char **)calloc(iFileNumInServer, sizeof(char *));
	
	for (i = 0; i < iFileNumInServer; ++i) {
		filesInServer[i] = (char *)calloc(MAX_FILE_NAME_CH_NUM, sizeof(char));
		typesOfFilesInServer[i] = (char *)calloc(MAX_TYPE_CH_NUM, sizeof(char));
	}
	
	for (i = 0; i < iFileNumInServer; ++i) {
		receiveDataOrResponseFromServer();
		strcpy(filesInServer[i], strServerResponse);
		
		receiveDataOrResponseFromServer();
		strcpy(typesOfFilesInServer[i], strServerResponse);
	}
	
	system("clear");
	gettimeofday(&stop, NULL); 
    printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, 
		stop.tv_usec - start.tv_usec);	
	
	printf("----- FILE EXPLORER IN SERVER -----\n");
	if (iFileNumInServer == 0)
		printf("There is no file in server!\n");				
	
	for (i = 0; i < iFileNumInServer; ++i) {
		printf(ANSI_COLOR_GREEN "File name: " ANSI_COLOR_RESET);
		printf(ANSI_COLOR_CYAN "%s " ANSI_COLOR_RESET ANSI_COLOR_RED "->" ANSI_COLOR_RESET
				ANSI_COLOR_YELLOW " Type of file: %s\n" ANSI_COLOR_RESET, filesInServer[i], 
				typesOfFilesInServer[i]);
	}
	
	for (i = 0; i < iFileNumInServer; ++i) {
		free(filesInServer[i]);
		free(typesOfFilesInServer[i]);
	}
		
	if (iFileNumInServer != 0) {
		free(filesInServer);
		free(typesOfFilesInServer);
	}
}

void listOnlineClients() {
	int i;
	
	sendDataOrRequestToServer(REQUEST_LIST_ONLINE_CLIENTS);
				
	receiveDataOrResponseFromServer();
	
	iOnlineUsersNum = atoi(strServerResponse);
	
	for (i = 0; i < iOnlineUsersNum; ++i) {
		receiveDataOrResponseFromServer();
		strcpy(onlineClients[i], strServerResponse);
	}
	
	system("clear");
	gettimeofday(&stop, NULL); 
    printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, 
		stop.tv_usec - start.tv_usec);
	printf("\n-----ONLINE CLIENTS -----\n");
	for (i = 0; i < iOnlineUsersNum; ++i) {
		printf(ANSI_COLOR_MAGENTA "Client id: " ANSI_COLOR_RESET);
		printf(ANSI_COLOR_YELLOW "%s\n" ANSI_COLOR_RESET, onlineClients[i]);
	}
}

void sendFile(char *searchedFile, char *clientId) {
	DIR *dir;
	struct dirent *sdirpEntry;
	char *cpSearchedFile;
	int iFileFounded = 0;
	
	if (!(dir = opendir(getcwd(NULL,0))))
		return;
	
	if (strcmp(searchedFile, "") == 0) {
		gettimeofday(&stop, NULL); 
        printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, 
			stop.tv_usec - start.tv_usec);
		printf(ANSI_COLOR_YELLOW "\nINFO - You haven't give any filename to send!\n" ANSI_COLOR_RESET);
		closedir(dir);
		return;
	}
		
	while ((sdirpEntry = readdir(dir)) != NULL) {
		if (strcmp(sdirpEntry->d_name,searchedFile) == 0) {
			iFileFounded = 1;
			break;
		} 
	}

	if (!iFileFounded) {
		gettimeofday(&stop, NULL); 
        printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
		printf(ANSI_COLOR_YELLOW "INFO - File %s can't be founded in %s\n" ANSI_COLOR_RESET, 
			searchedFile, getcwd(NULL, 0));
		closedir(dir);
		return;	
	} else if (strcmp(clientId, "") != 0) {
		gettimeofday(&stop, NULL); 
        printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, stop.tv_usec - start.tv_usec);
		printf(ANSI_COLOR_YELLOW "INFO - Sending file to client %s...\n" ANSI_COLOR_RESET,
			clientId);
		sendDataOrRequestToServer(REQUEST_CHECK_USER_ONLINE);
		sendDataOrRequestToServer(clientId);
		receiveDataOrResponseFromServer(strServerResponse);
		
		if (strcmp(strServerResponse, USER_ONLINE) == 0)
	    	sendFileToClient(searchedFile, clientId);
		else {
			gettimeofday(&stop, NULL); 
            printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET, 
				stop.tv_usec - start.tv_usec);
			printf(ANSI_COLOR_YELLOW "INFO - User offline...\n" ANSI_COLOR_RESET);
		}
	} else {
 		gettimeofday(&stop, NULL); 
        printf(ANSI_COLOR_CYAN "\nTime in milliseconds: %lu\n" ANSI_COLOR_RESET , 
			stop.tv_usec - start.tv_usec);
		printf(ANSI_COLOR_YELLOW "INFO - Sending file to your own server local...\n" ANSI_COLOR_RESET);
		sendFileToServersLocal(searchedFile);
	}
}

void sendFileToServersLocal(char *cpSendedFile) {	
	int iFileFD;
	char fileContentChunk[CHUNK_SIZE];
	
	iFileFD = open(cpSendedFile, O_RDONLY);	
	
	sendDataOrRequestToServer(REQUEST_SEND_FILE_TO_LOCAL);
	
	sendDataOrRequestToServer(cpSendedFile);
	
	while (read(iFileFD, fileContentChunk, sizeof(fileContentChunk)) != 0) {
		write(iCommunicationSocket, fileContentChunk, sizeof(fileContentChunk));
		
		read(iCommunicationSocket, strServerResponse, sizeof(strServerResponse));
			
		memset(fileContentChunk, 0, sizeof(fileContentChunk));
		memset(strServerResponse, 0, sizeof(strServerResponse));
	}
	
	write(iCommunicationSocket, END_PACKET, strlen(END_PACKET));
	
	read(iCommunicationSocket, strServerResponse, sizeof(strServerResponse));
		
	close(iFileFD);
}

void sendFileToClient(char *cpSendedFile, char *cpSendedClient) {
	int iFileFD;
	char fileContentChunk[CHUNK_SIZE];
	
	iFileFD = open(cpSendedFile, O_RDONLY);	
	
	sendDataOrRequestToServer(REQUEST_SEND_FILE_TO_CLIENT);
	
	sendDataOrRequestToServer(cpSendedFile);
	
	sendDataOrRequestToServer(cpSendedClient);
	
	while (read(iFileFD, fileContentChunk, sizeof(fileContentChunk)) != 0) {
		write(iCommunicationSocket, fileContentChunk, sizeof(fileContentChunk));
		
		read(iCommunicationSocket, strServerResponse, sizeof(strServerResponse));
			
		memset(fileContentChunk, 0, sizeof(fileContentChunk));
		memset(strServerResponse, 0, sizeof(strServerResponse));
	}
	
	write(iCommunicationSocket, END_PACKET, strlen(END_PACKET));
	
	read(iCommunicationSocket, strServerResponse, sizeof(strServerResponse));
		
	close(iFileFD);
}


void printCommandList() {
	printf("\n----- FTP SERVER COMMAND MANUAL -----\n");
	printf("lsLocal -> list local files in the directory client program started\n");
	printf("listServer -> to list the files in the current scope of the server-client\n");
	printf("lsClients -> lists the clients currently connected to the server with their respective clientids\n");
    printf("sendFile <fileName> <clientid> -> send the file <filename> (if file exists) from local ");
	printf("directory to the client with client id \"clientid\". If no client id is given the file ");
	printf("is send to the servers local directory.\n");
	printf("help -> displays the available commands and their usage\n\n");	
}

void sendDataOrRequestToServer(char *cpRequest) {
	// Send request to server
	sendedData = send(iCommunicationSocket, cpRequest, strlen(cpRequest), 0);
	
	// Receive ACKNOWLEDGE from server
	receivedData = recv(iCommunicationSocket, strServerResponse, sizeof(strServerResponse), 0);
	strServerResponse[receivedData] = '\0';			
}

void receiveDataOrResponseFromServer() {
	// Receive login check result from server	
	receivedData = recv(iCommunicationSocket, strServerResponse, sizeof(strServerResponse), 0);
	strServerResponse[receivedData] = '\0';
		
	// Send ACKNOWLEDGE to server
	sendedData = send(iCommunicationSocket, ACKNOWLEDGE, strlen(ACKNOWLEDGE), 0);
}

int checkRequestOrData(char *str) {
	if (strcmp(str, REQUEST_EXIT) == 0 || strcmp(str, REQUEST_LIST_ONLINE_CLIENTS) == 0 ||
		strcmp(str, REQUEST_LIST_SERVER_DIR) == 0 || strcmp(str, REQUEST_LOGIN_CHECK) == 0 ||
		strcmp(str, REQUEST_NEW_ACCOUNT_CHECK) == 0 || strcmp(str, REQUEST_SEND_FILE_TO_CLIENT) == 0 ||
		strcmp(str, REQUEST_SEND_FILE_TO_LOCAL) == 0)
		return REQUEST;
	else 
		return DATA;
}

ssize_t safeRead(int iFileDes, void *vpBuf, size_t size) {ssize_t retval;
	while ((retval = read(iFileDes, vpBuf, size)) == -1 && errno == EINTR);
	return retval;
}