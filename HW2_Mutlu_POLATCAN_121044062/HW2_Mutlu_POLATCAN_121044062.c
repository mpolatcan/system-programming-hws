/* CSE 244 - System Programming
   Mutlu POLATCAN - 121044062
   HW2 - grepFromDir */
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/wait.h>

#define MAX_PATH 256
#define OPEN_FILE O_WRONLY | O_APPEND
#define CREATE_FILE O_WRONLY | O_CREAT | O_APPEND
#define USER_PERMISSIONS S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH

void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize);
void readFileContent(int iFileDes, char **strArrFileContent, int iRowSize, int iColSize);
void findGivenString(char **strArrFileContent, char *cpFilename, char *cpGivenString, int iRowSize, int iColSize);
void getSubstring(char *cpSubstr, char *cpToken, int iBeginIndex, int iEndIndex);
int readLine(int iFileDes, char *cpBuffer, int iNBytes);
ssize_t writeLine(int iFileDes, void *vpBuf, size_t size);
void writeToLogFile(char *cpGivenString, char *cpFilename, int *intArrFoundedNum, int **intArrLineColumnNums, int iRowSize);
void wordFinder(char *cpFilename, char *cpGiventring);
pid_t r_wait(int *stat_loc);
void traverseDirectories(const char *cpCurDirPath, char *cpGivenString);

int main(int argc, char *argv[])
{
    if (argc < 3)
        fprintf(stderr,"Usage: %s [path] [string]\n",argv[0]);
    else
        traverseDirectories(argv[1], argv[2]);
    return 0;
}

pid_t r_wait(int *stat_loc) {
    int retval;

    while (((retval = wait(stat_loc)) == -1) && (errno == EINTR)) ;

    return retval;
}

void traverseDirectories(const char *cpCurDirPath, char *cpGivenString) {
    DIR *dir;
    pid_t childpid;
    char chArrNewDirPath[MAX_PATH];
    struct dirent *sdirpEntry;
    int i,
        iElementNumber = 0;

    if (!(dir = opendir(cpCurDirPath)))
        return;
    if (!(sdirpEntry = readdir(dir)))
        return;

    /* Counts elements number in current directory */
    while ((sdirpEntry = readdir(dir)) != NULL) {
        if (sdirpEntry->d_type == DT_DIR || sdirpEntry->d_type == DT_REG)
            ++iElementNumber;
    }

    rewinddir(dir);
    
    /* Create child process according to element number */
    for (i = 0; i < iElementNumber + 1; ++i) {
        sdirpEntry = readdir(dir);
        if ((childpid = fork()) == 0)
           break;  
    }

    while (r_wait(NULL) > 0); /* Wait all childrens */

    /* If this file is regular file send to word finder to find given word in file */
    if (sdirpEntry->d_type == DT_REG) {
        sprintf(chArrNewDirPath, "%s%s", cpCurDirPath, sdirpEntry->d_name);
        wordFinder(chArrNewDirPath, cpGivenString);
    } else if ((sdirpEntry->d_type == DT_DIR) && (strcmp(sdirpEntry->d_name, ".") != 0) &&
        (strcmp(sdirpEntry->d_name, "..") != 0)) {
        sprintf(chArrNewDirPath, "%s%s/", cpCurDirPath, sdirpEntry->d_name);
         /* Enter subdirectory of current directory */
        traverseDirectories(chArrNewDirPath, cpGivenString);
    }
}

/* This function finds given word in given file and writes to log file */
void wordFinder(char *cpFilename, char *cpGiventring)
{
    int i, iFileDes, iRowSize, iColSize;
    char **strArrFileContent;

    while ((iFileDes = open(cpFilename, O_RDONLY)) == -1 && errno == EINTR);

    if (iFileDes == -1)
        perror("File can't be opened!");
    else {
        /* Find row size and max column size of file */
        findRowAndColSize(iFileDes, &iRowSize, &iColSize);

        strArrFileContent = (char **)calloc(iRowSize + 1, sizeof(char *));
        for (i = 0; i < iRowSize; ++i)
            strArrFileContent[i] = (char *)calloc(iColSize + 1, sizeof(char));

        /* Read contents of file */
        readFileContent(iFileDes, strArrFileContent, iRowSize, iColSize);

        /* Find occurrences of given string and write information to log file */
        findGivenString(strArrFileContent, cpFilename, cpGiventring, iRowSize, iColSize);

        for (i = 0; i < iRowSize; ++i)
            free(strArrFileContent[i]);
        free(strArrFileContent);

        close(iFileDes);
    }
}

/* Find occurrences of given string, print to information to terminal and log file */
void findGivenString(char **strArrFileContent, char *cpFilename, char *cpGivenString, int iRowSize, int iColSize) {
    int i, 
        j,
        iFoundedNum = 0, 
        iTotalFoundedNum = 0,
        iCapacity = 0;  
    int **intArrLineColumnNums, *intArrFoundedNum;
    char *cpSubStr;

    cpSubStr = (char *)calloc(strlen(cpGivenString) + 1, sizeof(char));
    intArrLineColumnNums = (int **)calloc(iRowSize + 1, sizeof(int *)); // allocating to store column numbers of occurrences in a line
    intArrFoundedNum = (int *)calloc(iRowSize + 1, sizeof(int)); // allocating to store occurence number of given string in a line

    for (i = 0; i < iRowSize; ++i) {
        intArrLineColumnNums[i] = (int *)calloc(iCapacity + 20, sizeof(int));
        iCapacity += 20;
        if (strcmp(strArrFileContent[i], "\n") != 0) {      
            for (j = 0; j < strlen(strArrFileContent[i]); ++j) {
                getSubstring(cpSubStr, strArrFileContent[i], j, j + strlen(cpGivenString) - 1);
                if (strcmp(cpGivenString, cpSubStr) == 0) {
                    if (iFoundedNum == iCapacity) {
                        intArrLineColumnNums[i] = (int *)realloc(intArrLineColumnNums[i], (iCapacity + 20)*sizeof(int));
                        iCapacity += 20;
                    }
                    intArrLineColumnNums[i][iFoundedNum] = j + 1;
                    ++iFoundedNum;
                }
            }

            intArrFoundedNum[i] = iFoundedNum;
        }
        
        iCapacity = 0;
        iFoundedNum = 0;
    }
    
    for (i = 0; i < iRowSize; ++i)
        iTotalFoundedNum += intArrFoundedNum[i];
    
    if (iTotalFoundedNum != 0) {
        printf("\nOccurrences in file %s for \"%s\" string:\n", cpFilename, cpGivenString);
        printf("-------------------------------------------------------\n");
    }

    for (i = 0; i < iRowSize; ++i) {
        for (j = 0; j < intArrFoundedNum[i]; ++j) {
            if (j == 0) 
                printf("Line: %d | Number of occurrences in line: %d | Columns: { %d", i + 1, 
                    intArrFoundedNum[i], intArrLineColumnNums[i][j]);
            else
                printf(", %d", intArrLineColumnNums[i][j]);
        }
        if (intArrFoundedNum[i] != 0)
            printf(" }\n");
    }

    if (iTotalFoundedNum != 0)
        printf("\nTotal number of occurrences in file for string \"%s\": %d\n", cpGivenString, iTotalFoundedNum);

    if (iTotalFoundedNum == 0)
        printf("\nNo occurrences found in file %s for \"%s\"!\n", cpFilename, cpGivenString);

    writeToLogFile(cpGivenString, cpFilename, intArrFoundedNum, intArrLineColumnNums, iRowSize);

    for (i = 0; i < iRowSize; ++i)
        free(intArrLineColumnNums[i]);

    printf("-----------------------------------------------------------------------\n");

    free(intArrLineColumnNums);
    free(intArrFoundedNum);
    free(cpSubStr);
}

/* Writes search information to .log file */
void writeToLogFile(char *cpGivenString, char *cpFilename, int *intArrFoundedNum, int **intArrLineColumnNums, int iRowSize) {
    int i, 
        j, 
        iLogFileDes, 
        iRequiredChNum, 
        iTotalFoundedNum = 0;
    char chArrTmpBuffer[20];

    iLogFileDes = open("gfdLog.txt", CREATE_FILE, USER_PERMISSIONS);

    for (i = 0; i < iRowSize; ++i)
        iTotalFoundedNum += intArrFoundedNum[i];

    writeLine(iLogFileDes, "<---- Filename: ", 16);
    writeLine(iLogFileDes, cpFilename, strlen(cpFilename));
    writeLine(iLogFileDes, "     Searched String: ", 22);
    writeLine(iLogFileDes, cpGivenString, strlen(cpGivenString));
    writeLine(iLogFileDes, " ---->", 6);
    writeLine(iLogFileDes, "\n", 1);

    for (i = 0; i < iRowSize; ++i) {
        if (intArrFoundedNum[i] != 0) {
            writeLine(iLogFileDes, "Line", 4); 
            sprintf(chArrTmpBuffer, " %d", i +1);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
            writeLine(iLogFileDes, " | ", 3);
            writeLine(iLogFileDes, "Number of occurences in line: ", 30);
            sprintf(chArrTmpBuffer, "%d", intArrFoundedNum[i]);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
            writeLine(iLogFileDes, " | ", 3);
            writeLine(iLogFileDes, "Columns: { ", 11);
            sprintf(chArrTmpBuffer, "%d", intArrLineColumnNums[i][0]);
            writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
            
            for (j = 1; j < intArrFoundedNum[i]; ++j) {
                writeLine(iLogFileDes, ", ", 2);
                sprintf(chArrTmpBuffer, "%d", intArrLineColumnNums[i][j]);
                writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
            }
        
            if (intArrFoundedNum[i] != 0)
                writeLine(iLogFileDes, " }\n", 3);
        }
    }

    if (iTotalFoundedNum != 0) {
        writeLine(iLogFileDes, "Total number of occurrences in file: ", 37);
        sprintf(chArrTmpBuffer, "%d", iTotalFoundedNum);
        writeLine(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
        writeLine(iLogFileDes, "\n", 1);
    } else 
        writeLine(iLogFileDes, "No occurrences found in file\n", 30);
    
    writeLine(iLogFileDes, "\n*", 1);

    close(iLogFileDes);
}

/* Returns substring of given string according to begin and end index */
void getSubstring(char *cpSubStr, char *cpToken, int iBeginIndex, int iEndIndex) {
    int i, 
        j;

    for (i = 0, j = iBeginIndex; i < (iEndIndex - iBeginIndex) + 1, j < iEndIndex + 1; ++i, ++j)
        cpSubStr[i] = cpToken[j];
}

/* Reads contents of file according to row size and column size information */
void readFileContent(int iFileDes, char **strArrFileContent, int iRowSize, int iColSize) {
    int i;

    lseek(iFileDes, 0, SEEK_SET);

    for (i = 0; i < iRowSize; ++i) 
        readLine(iFileDes, strArrFileContent[i], iColSize);
}

/* Finds file's row size and max column size */
void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize) {
    char cNewline;
    int iTempColSize = 0;

    *piRowSize = 0;
    *piColSize = 0;

    while (read(iFileDes, &cNewline, 1)) {
        if (cNewline == '\n') {
            ++*piRowSize;
            
            if (*piColSize < iTempColSize)
                *piColSize = iTempColSize + 2;

            iTempColSize = 0;
        } else {
            ++iTempColSize;
        }
    }

    ++*piRowSize;
}

int readLine(int iFileDes, char *cpBuffer, int iNBytes) {
    int iNumRead = 0,
        iReturnValue;

    while (iNumRead < iNBytes - 1) {
        iReturnValue = read(iFileDes, cpBuffer + iNumRead, 1);
        if ((iReturnValue == -1) && (errno == EINTR))
            continue;
        if ((iReturnValue == 0) && (iNumRead == 0))
            return 0;
        if ((iReturnValue == 0))
            break;
        if (iReturnValue == -1)
            return -1;
        iNumRead++;
        if (cpBuffer[iNumRead-1] == '\n') {
            cpBuffer[iNumRead] = '\0';
            return iNumRead;
        }
    }

    errno = EINVAL;
    return -1;
}

ssize_t writeLine(int iFileDes, void *vpBuf, size_t size) {
    char *bufp;
    size_t bytestowrite;
    ssize_t byteswritten;
    size_t totalbytes;
    
    for (bufp = vpBuf, bytestowrite = size, totalbytes = 0;
         bytestowrite > 0;
         bufp += byteswritten, bytestowrite -= byteswritten) {
         byteswritten = write(iFileDes, bufp, bytestowrite);
        if ((byteswritten) == -1 && (errno != EINTR))
            return -1;
        if (byteswritten == -1)
            byteswritten = 0;
        totalbytes += byteswritten;
    }
    
    return totalbytes;
}