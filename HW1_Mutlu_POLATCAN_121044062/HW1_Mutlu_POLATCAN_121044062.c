/* CSE 244 - System Programming 
   HW1 - grepFromFile
   Mutlu Polatcan - 121044062 */
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define OPEN_FILE O_WRONLY | O_APPEND
#define CREATE_FILE O_WRONLY | O_CREAT | O_APPEND
#define USER_PERMISSIONS S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH

void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize);
void readFileContent(int iFileDes, char **strArrFileContent, int iRowSize, int iColSize);
void findGivenString(char **strArrFileContent, char *cpFilename, char *cpGivenString, int iRowSize, int iColSize);
void getSubstring(char *cpSubstr, char *cpToken, int iBeginIndex, int iEndIndex);
int readLine(int iFileDes, char *cpBuffer, int iNBytes);
void writeToLogFile(char *cpGivenString, char *cpFilename, int *intArrFoundedNum, int **intArrLineColumnNums, int iRowSize);

int main(int argc, char *argv[])
{
	int i, iFileDes, iRowSize, iColSize;
	char **strArrFileContent;

	if (argc != 3)
		printf("USAGE: ./grepFromFile <filename.xxx> <searchedString>\n");
	else {
	    while ((iFileDes = open(argv[1], O_RDONLY)) == -1 && errno == EINTR);

	    if (iFileDes == -1)
	    	perror("File can't be opened!");
	    else {
	    	/* Find row size and max column size of file */
		    findRowAndColSize(iFileDes, &iRowSize, &iColSize);

		    strArrFileContent = (char **)calloc(iRowSize + 1, sizeof(char *));
		    for (i = 0; i < iRowSize; ++i)
		    	strArrFileContent[i] = (char *)calloc(iColSize + 1, sizeof(char));

		    /* Read contents of file */
		    readFileContent(iFileDes, strArrFileContent, iRowSize, iColSize);
		    /* Find occurrences of given string and write information to log file */
		    findGivenString(strArrFileContent, argv[1], argv[2], iRowSize, iColSize);

		    for (i = 0; i < iRowSize; ++i)
		    	free(strArrFileContent[i]);
		    free(strArrFileContent);

		    close(iFileDes);
		}
	}

	return 0;
}

/* Find occurrences of given string, print to information to terminal and log file */
void findGivenString(char **strArrFileContent, char *cpFilename, char *cpGivenString, int iRowSize, int iColSize) {
	int i, 
		j,
		iFoundedNum = 0, 
		iTotalFoundedNum = 0,
		iCapacity = 0;	
	int **intArrLineColumnNums, *intArrFoundedNum;
	char *cpSubStr;

	cpSubStr = (char *)calloc(strlen(cpGivenString) + 1, sizeof(char));
	intArrLineColumnNums = (int **)calloc(iRowSize + 1, sizeof(int *)); // allocating to store column numbers of occurrences in a line
	intArrFoundedNum = (int *)calloc(iRowSize + 1, sizeof(int)); // allocating to store occurence number of given string in a line

	for (i = 0; i < iRowSize; ++i) {
		intArrLineColumnNums[i] = (int *)calloc(iCapacity + 20, sizeof(int));
		iCapacity += 20;
		if (strcmp(strArrFileContent[i], "\n") != 0) {		
			for (j = 0; j < strlen(strArrFileContent[i]); ++j) {
				getSubstring(cpSubStr, strArrFileContent[i], j, j + strlen(cpGivenString) - 1);
				if (strcmp(cpGivenString, cpSubStr) == 0) {
					if (iFoundedNum == iCapacity) {
						intArrLineColumnNums[i] = (int *)realloc(intArrLineColumnNums[i], (iCapacity + 20)*sizeof(int));
						iCapacity += 20;
					}
					intArrLineColumnNums[i][iFoundedNum] = j + 1;
					++iFoundedNum;
				}
			}

			intArrFoundedNum[i] = iFoundedNum;
		}
		
		iCapacity = 0;
		iFoundedNum = 0;
	}
	
	for (i = 0; i < iRowSize; ++i)
		iTotalFoundedNum += intArrFoundedNum[i];
	
	if (iTotalFoundedNum != 0) {
		printf("Occurrences in file for \"%s\" string:\n", cpGivenString);
		printf("-------------------------------------------------------\n\n");
	}

	for (i = 0; i < iRowSize; ++i) {
		for (j = 0; j < intArrFoundedNum[i]; ++j) {
			if (j == 0) 
				printf("Line: %d | Number of occurrences in line: %d | Columns: { %d", i + 1, 
					intArrFoundedNum[i], intArrLineColumnNums[i][j]);
			else
				printf(", %d", intArrLineColumnNums[i][j]);
		}
		if (intArrFoundedNum[i] != 0)
			printf(" }\n");
	}

	if (iTotalFoundedNum != 0)
		printf("\nTotal number of occurrences in file for string \"%s\": %d\n", cpGivenString, iTotalFoundedNum);

	if (iTotalFoundedNum == 0)
		printf("No occurrences found in file for \"%s\"!\n", cpGivenString);

	writeToLogFile(cpGivenString, cpFilename, intArrFoundedNum, intArrLineColumnNums, iRowSize);

	for (i = 0; i < iRowSize; ++i)
		free(intArrLineColumnNums[i]);

	free(intArrLineColumnNums);
	free(intArrFoundedNum);
	free(cpSubStr);
}

/* Writes search information to .log file */
void writeToLogFile(char *cpGivenString, char *cpFilename, int *intArrFoundedNum, int **intArrLineColumnNums, int iRowSize) {
	int i, 
		j, 
		iLogFileDes, 
		iRequiredChNum, 
		iTotalFoundedNum = 0;
	char chArrTmpBuffer[20];

	iLogFileDes = open("gffLog.txt", CREATE_FILE, USER_PERMISSIONS);

	for (i = 0; i < iRowSize; ++i)
		iTotalFoundedNum += intArrFoundedNum[i];

	write(iLogFileDes, "<---- Filename: ", 16);
	write(iLogFileDes, cpFilename, strlen(cpFilename));
	write(iLogFileDes, "     Searched String: ", 22);
	write(iLogFileDes, cpGivenString, strlen(cpGivenString));
	write(iLogFileDes, " ---->", 6);
	write(iLogFileDes, "\n", 1);

	for (i = 0; i < iRowSize; ++i) {
		if (intArrFoundedNum[i] != 0) {
			write(iLogFileDes, "Line", 4); 
			sprintf(chArrTmpBuffer, " %d", i +1);
			write(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
			write(iLogFileDes, " | ", 3);
			write(iLogFileDes, "Number of occurences in line: ", 30);
			sprintf(chArrTmpBuffer, "%d", intArrFoundedNum[i]);
			write(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
			write(iLogFileDes, " | ", 3);
			write(iLogFileDes, "Columns: { ", 11);
			sprintf(chArrTmpBuffer, "%d", intArrLineColumnNums[i][0]);
			write(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
			
			for (j = 1; j < intArrFoundedNum[i]; ++j) {
				write(iLogFileDes, ", ", 2);
				sprintf(chArrTmpBuffer, "%d", intArrLineColumnNums[i][j]);
				write(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
			}
		
			if (intArrFoundedNum[i] != 0)
				write(iLogFileDes, " }\n", 3);
		}
	}

	if (iTotalFoundedNum != 0) {
		write(iLogFileDes, "Total number of occurrences in file: ", 37);
		sprintf(chArrTmpBuffer, "%d", iTotalFoundedNum);
		write(iLogFileDes, chArrTmpBuffer, strlen(chArrTmpBuffer));
		write(iLogFileDes, "\n", 1);
	} else 
		write(iLogFileDes, "No occurrences found in file\n", 30);
	
	write(iLogFileDes, "\n*", 1);

	close(iLogFileDes);
}

/* Returns substring of given string according to begin and end index */
void getSubstring(char *cpSubStr, char *cpToken, int iBeginIndex, int iEndIndex) {
	int i, 
	    j;

	for (i = 0, j = iBeginIndex; i < (iEndIndex - iBeginIndex) + 1, j < iEndIndex + 1; ++i, ++j)
		cpSubStr[i] = cpToken[j];
}

/* Reads contents of file according to row size and column size information */
void readFileContent(int iFileDes, char **strArrFileContent, int iRowSize, int iColSize) {
	int i;

	lseek(iFileDes, 0, SEEK_SET);

	for (i = 0; i < iRowSize; ++i) 
		readLine(iFileDes, strArrFileContent[i], iColSize);
}

/* Finds file's row size and max column size */
void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize) {
	char cNewline;
	int iTempColSize = 0;

	*piRowSize = 0;
	*piColSize = 0;

	while (read(iFileDes, &cNewline, 1)) {
		if (cNewline == '\n') {
			++*piRowSize;
			
			if (*piColSize < iTempColSize)
				*piColSize = iTempColSize + 2;

			iTempColSize = 0;
		} else {
			++iTempColSize;
		}
	}

	++*piRowSize;
}

int readLine(int iFileDes, char *cpBuffer, int iNBytes) {
	int iNumRead = 0,
		iReturnValue;

	while (iNumRead < iNBytes - 1) {
		iReturnValue = read(iFileDes, cpBuffer + iNumRead, 1);
		if ((iReturnValue == -1) && (errno == EINTR))
			continue;
		if ((iReturnValue == 0) && (iNumRead == 0))
			return 0;
		if ((iReturnValue == 0))
			break;
		if (iReturnValue == -1)
			return -1;
		iNumRead++;
		if (cpBuffer[iNumRead-1] == '\n') {
			cpBuffer[iNumRead] = '\0';
			return iNumRead;
		}
	}

	errno = EINVAL;
	return -1;
}
