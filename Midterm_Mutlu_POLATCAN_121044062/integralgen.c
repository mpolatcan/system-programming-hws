/* Mutlu POLATCAN - 121044062
   CSE 244 - Midterm Project
   Client and Server Model - Integral Server Project
*/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <matheval.h>
#include <assert.h>

#define INTEGRAL_SERVER_MAIN_FIFO_NAME "IntegralGenMainServer"
#define INTEGRAL_SERVER_LOG_FILENAME "IntegralGenServerLog.txt"
#define CLIENT_LOG_FILE_PREFIX "ClientLogFile_"
#define FIFO_PERMS (S_IRWXU | S_IWGRP | S_IWOTH)
#define CREATE_FILE O_RDWR | O_CREAT
#define CREATE_SERVER_LOG_FILE O_WRONLY | O_CREAT | O_APPEND
#define USER_PERMISSIONS S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH
#define BILLION 1000000000L

int readLine(int iFileDes, char *cpBuffer, int iNBytes);
void readFiAndFjFiles(int iFiFileFD, int iFjFileFD, char *strArrFiFileContent,
					  char *strArrFjFileContent, int iFiFileColumnNum, int iFjFileColumnNum);
ssize_t safeRead(int iFileDes, void *vpBuf, size_t size);
ssize_t safeWrite(int iFileDes, void *vpBuf, size_t size);
void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize);
void findColSizeExpr(int iFileDes, int *piColSize);
pid_t safeWait(int *stat_loc);
double evaluator_evaluate_t(void *evaluator, double t);
void writeLogsOfClients();
void printClientInfo();

static volatile sig_atomic_t onlineClientNum = 0;

static int siClientNum = 0;

int iIntegralServerMainFifoFD,
	iIntegralSubserverFifoFD,
	iClientLogFileFD,
	iFiFileFD,
	iFjFileFD,
	iFiFileRowNum,
	iFjFileRowNum,
	iFiFileColumnNum,
	iFjFileColumnNum,
	iRequiredChNum;
char chOperation[PIPE_BUF], 
     strFirstOperand[PIPE_BUF],
     strSecondOperand[PIPE_BUF],
	 strSubserverFifoname[PIPE_BUF],
	 strCpySubserverFifoname[PIPE_BUF],
	 *strArrFiFileContent,
	 *strArrFjFileContent,
	 *cpClientLogFilename,
	 *cpShutdownReason,
	 *cpTimeIntervalTemp,
	 *cpFunctionTemp,
	 *cpResultTemp,
	 *cpExpression,
	 *cpPidTemp;	
double iTimeInterval;
pid_t *childpidArr;
struct tm *serverStartTimeInfo,
		  *clientEntranceTimeInfo,
		  *clientExitTimeInfo,
		  *calculateStartTimeInfo,
		  *calculateEndTimeInfo;
time_t serverStartTime,
	   clientEntranceTime,
	   clientExitTime,
	   calculateStartTime,
	   calculateEndTime;

/* Integral datas */
void *expression;
char **names;
int count;
double boundUpper,
	   boundLower,
	   timeInterval,
	   timeIntervalTemp,
	   resolutionInSec,
	   resultOfIntegral = 0,
	   h;
/* --------------- */

/* Set timer of server according to time interval parameter of client */
static int setServerTimer(double sec) {
	timer_t timerid;
	struct itimerspec value;

	if (timer_create(CLOCK_REALTIME, NULL, &timerid) == -1)
		return -1;

	value.it_interval.tv_sec = (long)sec;
	value.it_interval.tv_nsec = (sec - value.it_interval.tv_sec)*BILLION;
	
	if (value.it_interval.tv_nsec >= BILLION) {
		value.it_interval.tv_sec++;
		value.it_interval.tv_nsec -= BILLION;
	}
	
	value.it_value = value.it_interval;
	
	return timer_settime(timerid, 0, &value, NULL);
}

/* When SIGALRM signal comes, after time interval expires send result to 
corresponding client */
static void sendResultToClient(int signo, siginfo_t *info, void *context) {
	time(&calculateEndTime);
	calculateEndTimeInfo = localtime(&calculateEndTime);
	printf("Client: %ld - Result: %f - %s", (long)(getpid()-1),
		resultOfIntegral, asctime(calculateEndTimeInfo));

	write(iIntegralSubserverFifoFD, &resultOfIntegral, sizeof(double));
	write(iIntegralSubserverFifoFD, asctime(calculateEndTimeInfo), 
		strlen(asctime(calculateEndTimeInfo)));
}

/* Sets signal handler for SIGALRM signal */
static int setAlarm() {
	struct sigaction actionInterval;
	
	actionInterval.sa_flags = SA_SIGINFO;
	actionInterval.sa_sigaction = sendResultToClient;
	
	if ((sigemptyset(&actionInterval.sa_mask) == -1) ||
	    (sigaction(SIGALRM, &actionInterval, NULL) == -1))
		return -1;
	return 0;
}

/* Handler for SIGINT when server parent process gets a CTRL-C interrupt */
static void shutdownServer(int signo) {
	int i,
		j,
		iLogFileRowNum,
		iLogFileColumnNum,
		iClientLogFileFD,
		iIntegralServerLogFileFD;
	char *clientLogFilename;
	char **clientLogFileContent;

	printf("\nServer closed!\n");

	close(iIntegralServerMainFifoFD);
	kill(0, SIGUSR1); // send signals to child process for kill them and 
					  // logging their informations

	/* Remove main fifo of server */
	if (unlink(INTEGRAL_SERVER_MAIN_FIFO_NAME) == -1) {
		fprintf(stderr, "Server main fifo %s can't be unlinked\n", INTEGRAL_SERVER_MAIN_FIFO_NAME);
		exit(1);
	}

	/* Create IntegralGenServerLog.txt to save logs of each client */
	iIntegralServerLogFileFD = open(INTEGRAL_SERVER_LOG_FILENAME, CREATE_SERVER_LOG_FILE, 
								    USER_PERMISSIONS);

	/* Collect each client's log file and writes their content into one file which is 
	that IntegralGenServerLog.txt.Then remove each client's log file */
	for (i = 0; i < siClientNum; ++i) {
		iRequiredChNum = snprintf(NULL, 0, "%s%ld.txt", CLIENT_LOG_FILE_PREFIX, (long)childpidArr[i]);
		clientLogFilename = (char *)calloc(iRequiredChNum + 1, sizeof(char));
		sprintf(clientLogFilename, "%s%ld.txt", CLIENT_LOG_FILE_PREFIX, (long)childpidArr[i]);

		sleep(1);
		while ((iClientLogFileFD = open(clientLogFilename, O_RDONLY)) == -1);

		if (iClientLogFileFD == -1) {
			printf("Server can't be open %s\n file!", clientLogFilename);
			exit(1);
		}

		findRowAndColSize(iClientLogFileFD, &iLogFileRowNum, &iLogFileColumnNum);

		clientLogFileContent = (char **)calloc(iLogFileRowNum + 1, sizeof(char *));
		for (j = 0; j < iLogFileRowNum; ++j)
			clientLogFileContent[j] = (char *)calloc(iLogFileColumnNum + 1, sizeof(char));

		lseek(iClientLogFileFD, 0, SEEK_SET);

		for (j = 0; j < iLogFileRowNum; ++j)
			readLine(iClientLogFileFD, clientLogFileContent[j], iLogFileColumnNum);

		for (j = 0; j < iLogFileRowNum; ++j)
			write(iIntegralServerLogFileFD, clientLogFileContent[j], strlen(clientLogFileContent[j]));

		close(iClientLogFileFD);

		if (unlink(clientLogFilename) == -1) {
			printf("Server can't unlink the %s log file!\n", clientLogFilename);
			exit(1);
		}
		
		free(clientLogFilename);

		for (j = 0; j < iLogFileRowNum; ++j)
			free(clientLogFileContent[j]);
		free(clientLogFileContent);
	}

	close(iIntegralServerLogFileFD);

	free(childpidArr);
	free(cpShutdownReason);

	exit(1);
}

/* Handles signal SIGUSR1 when user shutdown and send signals its child */
static void killChildByServer(int signo) {
	iRequiredChNum = snprintf(NULL, 0, "Server shutdown by CTRL-C!");
	cpShutdownReason = (char *)calloc(iRequiredChNum + 1, sizeof(char));
	sprintf(cpShutdownReason, "Server shutdown by CTRL-C!");

	printf("Client %ld exited from server!\n", (long)getpid());
	close(iIntegralSubserverFifoFD);

	/* get pid of client from subserver fifoname and send interrupt to client */
	strcpy(strCpySubserverFifoname, strSubserverFifoname);
	strtok(strCpySubserverFifoname, "_");
	kill(atoi(strtok(NULL, "_")), SIGQUIT);
	usleep(500);	
	
	if (unlink(strSubserverFifoname) == -1) {
		fprintf(stderr, "Subserver FIFO %s can't be unlinked!", strSubserverFifoname);
		exit(1);
	}

	writeLogsOfClients();

	close(iFiFileFD);
	close(iFjFileFD);

	free(strArrFiFileContent);
	free(strArrFjFileContent);
	free(cpClientLogFilename);
	free(cpTimeIntervalTemp);
	free(cpPidTemp);
	free(cpShutdownReason);
	free(cpResultTemp);

	exit(1);
}

/* Handles SIGUSR2 signal when client exits from server and send SIGUSR2
signal its process */
static void killChildByClient(int signo) {
	iRequiredChNum = snprintf(NULL, 0, "CTRL-C event from client!");
	cpShutdownReason = (char *)calloc(iRequiredChNum + 1, sizeof(char));
	sprintf(cpShutdownReason, "CTRL-C event from client!");

	printf("\nClient %ld exited from server!\n", (long)getpid());

	close(iIntegralSubserverFifoFD);

	if (unlink(strSubserverFifoname) == -1) {
		fprintf(stderr, "Subserver FIFO %s can't be unlinked!", strSubserverFifoname);
		exit(1);

	}

	writeLogsOfClients();

	close(iFiFileFD);
	close(iFjFileFD);

	free(strArrFiFileContent);
	free(strArrFjFileContent);
	free(cpClientLogFilename);
	free(cpTimeIntervalTemp);
	free(cpPidTemp);
	free(cpShutdownReason);
	free(cpResultTemp);

	exit(1);
}

/* Handles SIGCHLD signal when comes to server's parent process 
about its child process died and then server's parent process decrement
online client number to take more client at the same time */
static void clientTerminated(int signo) {
	pid_t clientPid;
	int status;

	while((clientPid = waitpid(-1, &status, WNOHANG)) > 0)
		--onlineClientNum;
	
	if (onlineClientNum != 0)
		printf("\n%d clients are online!\n", onlineClientNum);
	else
		printf("\nNo clients are on server!\n");
}

int main(int argc, char *argv[])
{
	int i = 0,
		iCapacity;
	struct sigaction actionClientDied;

	/* Getting start time of server */
 	time(&serverStartTime);
	serverStartTimeInfo = localtime(&serverStartTime);
	printf("Server start time: %s\n", asctime(serverStartTimeInfo));

	if (argc != 3) {
		fprintf(stderr, "Usage: %s <resolution> <max # of clients>\n", argv[0]);
		exit(1);
	}

	resolutionInSec = atof(argv[1]) / 1000;
	printf("Resolution: %f\n", resolutionInSec);

	signal(SIGUSR1, SIG_IGN);
	signal(SIGINT, shutdownServer);

	if ((mkfifo(INTEGRAL_SERVER_MAIN_FIFO_NAME, FIFO_PERMS)) == -1 && (errno != EEXIST)) {
		fprintf(stderr, "IntegralGen server failed to create FIFO %s!\n", INTEGRAL_SERVER_MAIN_FIFO_NAME);
		exit(1);
	}

	while ((iIntegralServerMainFifoFD = open(INTEGRAL_SERVER_MAIN_FIFO_NAME, O_RDWR)) == -1 && errno == EINTR);

	if (iIntegralServerMainFifoFD == -1) {
		fprintf(stderr, "Server failed to open FIFO %s!\n", INTEGRAL_SERVER_MAIN_FIFO_NAME);
		exit(1);
	}

	/* Setting SIGCHLD signal handler */
    memset(&actionClientDied, 0, sizeof(actionClientDied));
    actionClientDied.sa_handler = clientTerminated;
    sigaction(SIGCHLD, &actionClientDied, NULL);

    /* Allocating memory for pids to reach each client's log file */
    childpidArr = (pid_t *)calloc(atoi(argv[2]), sizeof(pid_t));
    iCapacity = atoi(argv[2]); // initial capacity is setted to max number of clients

	for (;;) {
		for (;onlineClientNum <= atoi(argv[2]);) {
			if (onlineClientNum == atoi(argv[2])) { 
				// if server is full, don't take another clients to server.
				// Also unnecessary data stay in fifo so you need to read it
				// to delete from fifo.Also that is like flushing fifo 
				safeRead(iIntegralServerMainFifoFD, strSubserverFifoname, PIPE_BUF);
				printf("\nServer is full!\n\n");
			} else {
				safeRead(iIntegralServerMainFifoFD, strSubserverFifoname, PIPE_BUF);
				++onlineClientNum;

				/* if number of clients which has logged into server is equal to max number of clients
				expand memory */
				if (siClientNum == atoi(argv[2])) {
					iCapacity += 10; // increase capacity by 10 
					childpidArr = (pid_t *)realloc(childpidArr, iCapacity * sizeof(pid_t));
				}
				/*----------------------------------------------------------------------------------*/
				
				
				if ((childpidArr[siClientNum] = fork()) == 0)
					break;

				++siClientNum;
			}
		}

		if (childpidArr[siClientNum] == 0) {
			int i;

			signal(SIGINT, SIG_IGN);
			signal(SIGUSR1, killChildByServer);
			signal(SIGUSR2, killChildByClient);

			time(&clientEntranceTime);
			clientEntranceTimeInfo = localtime(&clientEntranceTime);
			
			if (mkfifo(strSubserverFifoname, FIFO_PERMS) == -1 && (errno != EEXIST)) {
				fprintf(stderr, "IntegralGen main server failed to create sub server fifo %s!\n", strSubserverFifoname);
				exit(1);
			}

			while ((iIntegralSubserverFifoFD = open(strSubserverFifoname, O_RDWR)) == -1 && errno == EINTR);

			if (iIntegralSubserverFifoFD == -1) {
				fprintf(stderr, "Server failed to open subserver FIFO %s!\n", strSubserverFifoname);
				exit(1);
			}

			printf("\n----- CLIENT %d -----\n", siClientNum + 1);
			printf("Client entrance time %s", asctime(clientEntranceTimeInfo));
			printf("Client connected to subserver: %s\n", strSubserverFifoname);
			printf("Process pid: %ld\n", (long)getpid());
			printf("Parent pid: %ld\n", (long)getppid());

			safeRead(iIntegralSubserverFifoFD, strFirstOperand, PIPE_BUF);
			safeRead(iIntegralSubserverFifoFD, strSecondOperand, PIPE_BUF);
			safeRead(iIntegralSubserverFifoFD, &iTimeInterval, sizeof(double));
			safeRead(iIntegralSubserverFifoFD, chOperation, PIPE_BUF);

			printf("First operand's function name: %s\n", strFirstOperand);
			printf("Second operand's function name: %s\n", strSecondOperand);
			printf("Time Interval: %f\n", iTimeInterval);
			printf("Operation: %s\n", chOperation);

			sprintf(strFirstOperand, "%s.txt", strFirstOperand);
			sprintf(strSecondOperand, "%s.txt", strSecondOperand);

			while ((iFiFileFD = open(strFirstOperand, O_RDONLY)) == -1 && errno == EINTR);

			if (iFiFileFD == -1) {
				fprintf(stderr, "File %s can't be opened!\n", strFirstOperand);
				exit(1);
			}

			while ((iFjFileFD = open(strSecondOperand, O_RDONLY)) == -1 && errno == EINTR);

			if (iFjFileFD == -1) {
				fprintf(stderr, "File %s can't be opened!\n", strSecondOperand);
				close(iFiFileFD);
				exit(1);
			}

			/* Find row and column numbers of files fi.txt and fj.txt */
			findColSizeExpr(iFiFileFD, &iFiFileColumnNum);
			findColSizeExpr(iFjFileFD, &iFjFileColumnNum);

			strArrFiFileContent = (char *)calloc(iFiFileColumnNum + 1, sizeof(char));
			strArrFjFileContent = (char *)calloc(iFjFileColumnNum + 1, sizeof(char));

			/* Read fi.txt and fj.txt files */
			readFiAndFjFiles(iFiFileFD, iFjFileFD, strArrFiFileContent, strArrFjFileContent,
							 iFiFileColumnNum, iFjFileColumnNum);

			printf("Function: %s %s %s\n", strArrFiFileContent, chOperation, strArrFjFileContent);
			printf("--------------------\n\n");

			iRequiredChNum = snprintf(NULL, 0, "%s %s %s", strArrFiFileContent, chOperation, 
									 strArrFjFileContent);
			cpExpression = (char *)calloc(iRequiredChNum + 1, sizeof(char));
			sprintf(cpExpression, "%s %s %s", strArrFiFileContent, chOperation,
				strArrFjFileContent);

			expression = evaluator_create(cpExpression);
			assert(expression);

			timeIntervalTemp = iTimeInterval;
			boundLower = difftime(clientEntranceTime, serverStartTime);
			h = iTimeInterval / resolutionInSec;
			boundUpper = boundLower + h;
			timeIntervalTemp = iTimeInterval + boundUpper;

			evaluator_get_variables(expression, &names, &count);

			close(iFiFileFD);
			close(iFjFileFD);

			/* setting signal handler for SIGALRM */
			if (setAlarm() == -1) {
				perror("Failed to setup SIGALRM handler");
				return 1;
			}

			/* setting timer for time interval */
			if (setServerTimer(iTimeInterval) == -1) {
				perror("Failed to setup periodic interrupt");
				return 1;
			}

			time(&calculateStartTime);
			calculateStartTimeInfo = localtime(&calculateStartTime);
			printf("Server started calculation at: %s\n", asctime(calculateStartTimeInfo));

			for (;;) {
				/* Trapezoidal rule to calculate integral of expression */
				for (; boundUpper < timeIntervalTemp;) {
					resultOfIntegral += h * ((evaluator_evaluate_t(expression, boundLower) + 
							evaluator_evaluate_t(expression, boundUpper)) / 2);
					boundUpper += h;
					boundLower += h;
				}

				pause();

				timeIntervalTemp += iTimeInterval;
			}

			evaluator_destroy(expression);
		}
	}

	return 0;	
}

double evaluator_evaluate_t(void *evaluator, double t) {
	char *names[] = { "t" };
	double values[] = { t };

	return evaluator_evaluate(evaluator, sizeof(names) / sizeof(names[0]), names, values);
}

/* Writes log files for each client which has been logged into server */
void writeLogsOfClients() {
	/* Maintain a log file for each child process */ 
	iRequiredChNum = snprintf(NULL, 0, "%s%ld", CLIENT_LOG_FILE_PREFIX, (long)getpid());
	cpClientLogFilename = (char *)calloc(iRequiredChNum + 1, sizeof(char));
	sprintf(cpClientLogFilename, "%s%ld.txt", CLIENT_LOG_FILE_PREFIX, (long)getpid());
	iClientLogFileFD = open(cpClientLogFilename, CREATE_FILE, USER_PERMISSIONS);

	iRequiredChNum = snprintf(NULL, 0, "%ld", (long)getpid());
	cpPidTemp = (char *)calloc(iRequiredChNum + 1, sizeof(char));
	sprintf(cpPidTemp, "%ld", (long)getpid());

	iRequiredChNum = snprintf(NULL, 0, "%f", iTimeInterval);
	cpTimeIntervalTemp = (char *)calloc(iRequiredChNum + 1, sizeof(char));
	sprintf(cpTimeIntervalTemp, "%f", iTimeInterval);

	iRequiredChNum = snprintf(NULL, 0, "%s %s %s", strArrFiFileContent, chOperation,
		strArrFjFileContent);
	cpFunctionTemp = (char *)calloc(iRequiredChNum + 1, sizeof(char));
	sprintf(cpFunctionTemp, "%s %s %s", strArrFiFileContent, chOperation, 
		strArrFjFileContent);

	iRequiredChNum = snprintf(NULL, 0, "%f", resultOfIntegral);
	cpResultTemp = (char *)calloc(iRequiredChNum + 1, sizeof(char));
	sprintf(cpResultTemp, "%f", resultOfIntegral);

	safeWrite(iClientLogFileFD, "------ CLIENT ", strlen("------ CLIENT "));
	safeWrite(iClientLogFileFD, cpPidTemp, strlen(cpPidTemp));
	safeWrite(iClientLogFileFD, " -----\n", strlen(" -----\n"));
	safeWrite(iClientLogFileFD, "Entrance time: ", strlen("Entrance time: "));
	safeWrite(iClientLogFileFD, asctime(clientEntranceTimeInfo), 
		strlen(asctime(clientEntranceTimeInfo)));

	time(&clientExitTime);
	clientExitTimeInfo = localtime(&clientExitTime);

	safeWrite(iClientLogFileFD, "Exit time: ", strlen("Exit time: "));
	safeWrite(iClientLogFileFD, asctime(clientExitTimeInfo), 
		strlen(asctime(clientExitTimeInfo)));
	safeWrite(iClientLogFileFD, "First operand: ", strlen("First operand: "));
	safeWrite(iClientLogFileFD, strFirstOperand, strlen(strFirstOperand));
	safeWrite(iClientLogFileFD, "\n", strlen("\n"));
	safeWrite(iClientLogFileFD, "Second operand: ", strlen("Second operand: "));
	safeWrite(iClientLogFileFD, strSecondOperand, strlen(strSecondOperand));
	safeWrite(iClientLogFileFD, "\n", strlen("\n"));
	safeWrite(iClientLogFileFD, "Time interval: ", strlen("Time interval: "));
	safeWrite(iClientLogFileFD, cpTimeIntervalTemp, strlen(cpTimeIntervalTemp));
	safeWrite(iClientLogFileFD, "\n", strlen("\n"));
	safeWrite(iClientLogFileFD, "Function: ", strlen("Function: "));
	safeWrite(iClientLogFileFD, cpFunctionTemp, strlen(cpFunctionTemp));
	safeWrite(iClientLogFileFD, "\n", strlen("\n"));
	safeWrite(iClientLogFileFD, "Result: ", strlen("Result: "));
	safeWrite(iClientLogFileFD, cpResultTemp, strlen(cpResultTemp));
	safeWrite(iClientLogFileFD, "\n", strlen("\n"));
	safeWrite(iClientLogFileFD, "Exit reason: ", strlen("Exit reason: "));
	safeWrite(iClientLogFileFD, cpShutdownReason, strlen(cpShutdownReason));
	safeWrite(iClientLogFileFD, "\n\n", strlen("\n\n"));

	close(iClientLogFileFD);
}

/* Read contents of fi and fj files */
void readFiAndFjFiles(int iFiFileFD, int iFjFileFD, char *strArrFiFileContent,
					  char *strArrFjFileContent, int iFiFileColumnNum, int iFjFileColumnNum) {
	lseek(iFiFileFD, 0, SEEK_SET);
	lseek(iFjFileFD, 0, SEEK_SET);

	readLine(iFiFileFD, strArrFiFileContent, iFiFileColumnNum + 1);
	readLine(iFjFileFD, strArrFjFileContent, iFjFileColumnNum + 1);
}

/* Finds file's row size and max column size */
void findRowAndColSize(int iFileDes, int *piRowSize, int *piColSize) {
	char cNewline;
	int iTempColSize = 0;

	*piRowSize = 0;
	*piColSize = 0;

	while (safeRead(iFileDes, &cNewline, 1)) {
		if (cNewline == '\n') {
			++*piRowSize;
			
			if (*piColSize < iTempColSize)
				*piColSize = iTempColSize + 2;

			iTempColSize = 0;
		} else {
			++iTempColSize;
		}
	}

	++*piRowSize;
}

/* find column size of expression and fi.txt and fj.txt files */
void findColSizeExpr(int iFileDes, int *piColSize) {
	char cNewline;

	*piColSize = 0;

	while (safeRead(iFileDes, &cNewline, 1)) {
		if (cNewline != '\n')
			++*piColSize;
	}	
}

int readLine(int iFileDes, char *cpBuffer, int iNBytes) {
	int iNumRead = 0,
		iReturnValue;

	while (iNumRead < iNBytes - 1) {
		iReturnValue = read(iFileDes, cpBuffer + iNumRead, 1);
		if ((iReturnValue == -1) && (errno == EINTR))
			continue;
		if ((iReturnValue == 0) && (iNumRead == 0))
			return 0;
		if ((iReturnValue == 0))
			break;
		if (iReturnValue == -1)
			return -1;
		iNumRead++;
		if (cpBuffer[iNumRead-1] == '\n') {
			cpBuffer[iNumRead] = '\0';
			return iNumRead;
		}
	}

	errno = EINVAL;
	return -1;
}

pid_t safeWait(int *stat_loc) {
	int retval;
	while (((retval = wait(stat_loc)) == -1) && (errno == EINTR)) ;
	return retval;
}

ssize_t safeRead(int iFileDes, void *vpBuf, size_t size) {ssize_t retval;
	while ((retval = read(iFileDes, vpBuf, size)) == -1 && errno == EINTR);
	return retval;
}

ssize_t safeWrite(int iFileDes, void *vpBuf, size_t size) {
    char *bufp;
    size_t bytestowrite;
    ssize_t byteswritten;
    size_t totalbytes;
    
    for (bufp = vpBuf, bytestowrite = size, totalbytes = 0;
         bytestowrite > 0;
         bufp += byteswritten, bytestowrite -= byteswritten) {
         byteswritten = write(iFileDes, bufp, bytestowrite);
        if ((byteswritten) == -1 && (errno != EINTR))
            return -1;
        if (byteswritten == -1)
            byteswritten = 0;
        totalbytes += byteswritten;
    }
    
    return totalbytes;
}