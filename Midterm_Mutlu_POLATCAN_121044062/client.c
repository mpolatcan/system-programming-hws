/* Mutlu POLATCAN - 121044062
   CSE 244 - Midterm Project
   Client and Server Model - Integral Server Project
*/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <limits.h>

#define CORRECT_OPERATION 0
#define INCORRECT_OPERATION -1
#define CORRECT_FUNCTION_NAME 0
#define INCORRECT_FUNCTION_NAME -1
#define INTEGRAL_SERVER_MAIN_FIFO_NAME "IntegralGenMainServer"
#define SUB_SERVER_PREFIX "IntegralGenSubServer_"

ssize_t safeRead(int iFileDes, void *vpBuf, size_t size);
ssize_t safeWrite(int iFileDes, void *vpBuf, size_t size);
void readFiAndFjFiles(int iFiFileDes, int iFjFileDes, char **strArrFiFileContent, 
					  char **strArrFjFileContent, int iFiFileRowNum, int iFiFileColumnNum,
					  int iFjFileRowNum, int iFjFileColumnNum);
int checkOperation(char *cpOperation);
int checkFunctionName(char *cpFunctionName);

int iIntegralServerMainFifoFD,
	iIntegralSubserverFifoFD;
char *cpSubserverFifoName;

/* When user send CTRL-C signal then notify your process
which is in server that client exiting from server and kill
that process */
static void terminatedByUser(int signo) {
    close(iIntegralServerMainFifoFD);

    kill((long)(getpid()+1), SIGUSR2);

	close(iIntegralSubserverFifoFD);

    free(cpSubserverFifoName);

    exit(1);
}

/* When server shutdowns notify the client that 
server is closed then kill the client */
static void terminatedByServer(int signo) {
 	close(iIntegralServerMainFifoFD);

	close(iIntegralSubserverFifoFD);

    free(cpSubserverFifoName);

    printf("Server closed!\n");

    exit(1);
}

int main(int argc, char *argv[]) {
	int i,
		iRequiredChNum;
	double iTimeInterval,
		   iIntegralResult;
	struct sigaction action;
	char strGettingTime[PIPE_BUF];
		
	if (argc != 5) {
		fprintf(stderr, "Usage: %s <fi> <fj> <time interval> <operation>\n", argv[0]);
		exit(1);
	}

	signal(SIGINT, terminatedByUser);
	signal(SIGQUIT, terminatedByServer);

	while ((iIntegralServerMainFifoFD = open(INTEGRAL_SERVER_MAIN_FIFO_NAME, O_RDWR)) == -1 && errno == EINTR);

	if (iIntegralServerMainFifoFD == -1) {
		fprintf(stderr, "HTTP 404 - Server not found!\n");
		fprintf(stderr, "Client failed to connect integral server!\n");
		exit(1);
	}		
	
	if (checkOperation(argv[4]) != CORRECT_OPERATION) {
		fprintf(stderr, "You have entered incorrect operation! Please try again later!\n");
		close(iIntegralServerMainFifoFD);
		exit(1);
	} else if (checkFunctionName(argv[1]) != CORRECT_FUNCTION_NAME ||
			   checkFunctionName(argv[2]) != CORRECT_OPERATION) {
		fprintf(stderr, "You have entered incorrect operation! Please try again later!\n");
		close(iIntegralServerMainFifoFD);
		exit(1);
	} 

	iTimeInterval = atof(argv[3]);

	iRequiredChNum = snprintf(NULL, 0, "%s%ld", SUB_SERVER_PREFIX, (long)getpid);
	cpSubserverFifoName = (char *)calloc(iRequiredChNum, sizeof(char));
	sprintf(cpSubserverFifoName, "%s%ld", SUB_SERVER_PREFIX, (long)getpid());
	
	safeWrite(iIntegralServerMainFifoFD, cpSubserverFifoName, strlen(cpSubserverFifoName));
		
	close(iIntegralServerMainFifoFD);

	sleep(1);

	while ((iIntegralSubserverFifoFD = open(cpSubserverFifoName, O_RDWR)) == -1 && errno == EINTR);

	if (iIntegralSubserverFifoFD == -1) {
		fprintf(stderr, "HTTP 3503 - Server too busy!\n");
		fprintf(stderr, "Client failed to connect integral server. Because server is full\n");
				
		free(cpSubserverFifoName);

		exit(1);
	}

	printf("First: %s\n", argv[1]);
	printf("Second: %s\n", argv[2]);
	printf("Time interval: %f\n", atof(argv[3]));
	printf("Operation: %s\n\n", argv[4]);
	
	safeWrite(iIntegralSubserverFifoFD, argv[1], strlen(argv[1]));
	usleep(500);
	safeWrite(iIntegralSubserverFifoFD, argv[2], strlen(argv[2]));
	usleep(500);
	safeWrite(iIntegralSubserverFifoFD, &iTimeInterval, sizeof(double));
	usleep(500);
	safeWrite(iIntegralSubserverFifoFD, argv[4], PIPE_BUF);
	usleep(500);

	for (;;) { 
		read(iIntegralSubserverFifoFD, &iIntegralResult, sizeof(double));
		read(iIntegralSubserverFifoFD, strGettingTime, PIPE_BUF);
		printf("Result: %f - %s", iIntegralResult, strGettingTime);
	}
}

int checkOperation(char *cpOperation) {
	if (strcmp(cpOperation,"+") == 0 || 
		strcmp(cpOperation,"-") == 0 || 
		strcmp(cpOperation, "/") == 0 || 
		strcmp(cpOperation, "*") == 0)
		return CORRECT_OPERATION;
	else
		return INCORRECT_OPERATION;
}

int checkFunctionName(char *cpFunctionName) {
	if (strcmp(cpFunctionName, "f1") == 0 ||
		strcmp(cpFunctionName, "f2") == 0 ||
		strcmp(cpFunctionName, "f3") == 0 ||
		strcmp(cpFunctionName, "f4") == 0 ||
		strcmp(cpFunctionName, "f5") == 0 ||
		strcmp(cpFunctionName, "f6") == 0)
		return CORRECT_FUNCTION_NAME;
	else
		return INCORRECT_FUNCTION_NAME;
}

ssize_t safeRead(int iFileDes, void *vpBuf, size_t size) {
	ssize_t retval;
	while ((retval = read(iFileDes, vpBuf, size)) == -1 && errno == EINTR);
	return retval;
}

ssize_t safeWrite(int iFileDes, void *vpBuf, size_t size) {
    char *bufp;
    size_t bytestowrite;
    ssize_t byteswritten;
    size_t totalbytes;
    
    for (bufp = vpBuf, bytestowrite = size, totalbytes = 0;
         bytestowrite > 0;
         bufp += byteswritten, bytestowrite -= byteswritten) {
         byteswritten = write(iFileDes, bufp, bytestowrite);
        if ((byteswritten) == -1 && (errno != EINTR))
            return -1;
        if (byteswritten == -1)
            byteswritten = 0;
        totalbytes += byteswritten;
    }
    
    return totalbytes;
}